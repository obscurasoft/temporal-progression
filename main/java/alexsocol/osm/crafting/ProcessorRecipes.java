package alexsocol.osm.crafting;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishFood;
import net.minecraft.item.ItemStack;

public class ProcessorRecipes
{
    private static final ProcessorRecipes processingBase = new ProcessorRecipes();
    private Map processingList = new HashMap();
    private Map experienceList = new HashMap();
    private static final String __OBFID = "CL_00000085";

    public static ProcessorRecipes processing()
    {
        return processingBase;
    }

    private ProcessorRecipes()
    {
        this.registerItem(Items.potato, new ItemStack(Items.diamond), 2.35F);
    }

    public void registerBlock(Block block, ItemStack item, float xp)
    {
        this.registerItem(Item.getItemFromBlock(block), item, xp);
    }

    public void registerItem(Item item, ItemStack stack, float xp)
    {
        this.addRecipe(new ItemStack(item, 1, 32767), stack, xp);
    }

    public void addRecipe(ItemStack item, ItemStack stack, float xp)
    {
        this.processingList.put(item, stack);
        this.experienceList.put(stack, Float.valueOf(xp));
    }

    public ItemStack getProcessingResult(ItemStack stack)
    {
        Iterator iterator = this.processingList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return null;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.match(stack, (ItemStack)entry.getKey()));

        return (ItemStack)entry.getValue();
    }

    private boolean match(ItemStack stack, ItemStack stack2)
    {
        return stack2.getItem() == stack.getItem() && (stack2.getItemDamage() == 32767 || stack2.getItemDamage() == stack.getItemDamage());
    }

    public Map getProcessingList()
    {
        return this.processingList;
    }

    public float giveExp(ItemStack stack)
    {
        float ret = stack.getItem().getSmeltingExperience(stack);
        if (ret != -1) return ret;

        Iterator iterator = this.experienceList.entrySet().iterator();
        Entry entry;

        do
        {
            if (!iterator.hasNext())
            {
                return 0.0F;
            }

            entry = (Entry)iterator.next();
        }
        while (!this.match(stack, (ItemStack)entry.getKey()));

        return ((Float)entry.getValue()).floatValue();
    }
}
