package alexsocol.osm;

import java.io.File;

import net.minecraft.potion.Potion;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class OSMConfig extends Configuration {
	private final String KEY_allowCreativeTargets = "Allow_Creative_Targets";

	private static final String CATEGORY_BETA = "beta";
	private static final String CATEGORY_POTIONS = "potions";

	public static final String DEFAULT_LANGUAGE = "en_US";

	public OSMConfig(File file) {
		super(file);
		load();
		addCustomCategoryComment(CATEGORY_BETA, "InDev");
		addCustomCategoryComment(CATEGORY_POTIONS, "InDev");
	}

	public void init() {
		initDirectProperties();
		save();
	}

	public void clientInit() {
		save();
	}

	// ====================================================================================
	// Getters - Direct
	// ====================================================================================
	// ping the direct properties once so that they show up in config
	public void initDirectProperties() {
	}

	// Gets the first available potion ID
	// returns -1 if there are no available IDs
	private static int getNextFreePotionID() {
		int freeID = -1;
		for (int i = 1; i < Potion.potionTypes.length; i++) {
			if (Potion.potionTypes[i] == null) {
				freeID = i;
				break;
			}
		}
		return freeID;
	}

	public int getConfigurablePotionID(String id, int default_value) {
		// because we auto-configure, default_value is ignored
		int val = getNextFreePotionID();

		if (val == -1 && !hasKey(CATEGORY_POTIONS, id)) {
			LogHelper.error("Cannot find a free potion ID for the %s effect. This will cause severe problems!", id);
			LogHelper.error(
					"Effect %s has been assigned to a default of potion ID 1 (movement speed). Erroneous behaviour *will* result.",
					id);
			val = 1;
		}
		Property prop = get(CATEGORY_POTIONS, id, val);
		val = prop.getInt(val);
		save();
		return val;
	}
}
