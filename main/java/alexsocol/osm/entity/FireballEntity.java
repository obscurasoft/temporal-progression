package alexsocol.osm.entity;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.particle.EntityLargeExplodeFX;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class FireBallEntity extends EntityLargeFireball{
	
	public FireBallEntity(World world) {
		super(world);
	}
	
	public FireBallEntity(World world, double par2, double par4, double par6, double par8, double par10, double par12) {
		super(world, par2, par4, par6, par8, par10, par12);
		this.shootingEntity = null;
	}
	
	@Override
	protected void onImpact(MovingObjectPosition mop1)
    {
        if (!this.worldObj.isRemote)
        {
            if (mop1.entityHit != null)
            {
            	mop1.entityHit.attackEntityFrom(DamageSource.causeFireballDamage(this, this.shootingEntity), 6.0F);
            }
            
            for (int i = 0; i < 4; i++){
            	this.worldObj.spawnParticle("lava", this.posX, this.posY, this.posZ, 0.0, 0.0, 0.0);
            }
            
            this.worldObj.newExplosion((Entity)null, this.posX, this.posY, this.posZ, (float)this.field_92057_e, true, false);
            this.setDead();
        }
    }
	
	@Override
	public void onUpdate(){
		super.onUpdate();
		for (int i = 0; i < 4; ++i)
        {
            this.worldObj.spawnParticle("largesmoke", this.posX + this.motionX * (double)i / 4.0D, this.posY + this.motionY * (double)i / 4.0D, this.posZ + this.motionZ * (double)i / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
            this.worldObj.spawnParticle("flame", this.posX + this.motionX * (double)i / 4.0D, this.posY + this.motionY * (double)i / 4.0D, this.posZ + this.motionZ * (double)i / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
            this.worldObj.spawnParticle("lava", this.posX + this.motionX * (double)i / 4.0D, this.posY + this.motionY * (double)i / 4.0D, this.posZ + this.motionZ * (double)i / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
        }
	}
}
