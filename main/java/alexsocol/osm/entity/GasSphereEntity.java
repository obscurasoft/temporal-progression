package alexsocol.osm.entity;

import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class GasSphereEntity extends EntityThrowable {
	
    public GasSphereEntity(World world) {
        super(world);
    }

    public GasSphereEntity(World world, EntityLivingBase living) {
        super(world, living);
    }

    public GasSphereEntity(World world, double d2, double d4, double d6) {
        super(world, d2, d4, d6);
    }
    
    public int distanceTo(int xa, int ya, int za, int bx, int yb, int zb) { 
		double d0 = (xa + 0.5D) - (bx + 0.5D); 
		double d1 = (ya + 0.5D) - (yb + 0.5D); 
		double d2 = (za + 0.5D) - (zb + 0.5D); 
		return (int)Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
    }
    
    protected void onImpact(MovingObjectPosition mop) {
        if (!this.worldObj.isRemote) {
        	int x = (int)this.posX; if (x < 0){--x;}
        	int y = (int)this.posY; if (y < 0){--y;}
        	int z = (int)this.posZ; if (z < 0){--z;}
        	
        	int radius = 2; 

        	for (int rx = x - radius; rx <= x + radius; ++rx) { 
        		for (int ry = y - radius; ry <= y + radius; ++ry) { 
        			for (int rz = z - radius; rz <= z + radius; ++rz) { 
        				if (this.worldObj.isAirBlock(rx, ry, rz) && distanceTo(x, y, z, rx, ry, rz) <= radius){ 
        					this.worldObj.setBlock(rx, ry, rz, CommonProxy.gasCloud);
        				}
        			} 
        		} 
        	}
        	this.setDead();
        }
    }
}
