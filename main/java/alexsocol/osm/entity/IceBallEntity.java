package alexsocol.osm.entity;

import java.util.Random;

import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class IceBallEntity extends EntityThrowable {
	
	private final Random random = new Random();
	
	public IceBallEntity(World world) {
        super(world);
    }

    public IceBallEntity(World world, EntityLivingBase living) {
        super(world, living);
    }

    public IceBallEntity(World world, double d2, double d4, double d6) {
        super(world, d2, d4, d6);
    }
    
    protected void onImpact(MovingObjectPosition mop) {
        if (!this.worldObj.isRemote) {
        	int x = (int)this.posX; if (x < 0){--x;}
        	int y = (int)this.posY; if (y < 0){--y;}
        	int z = (int)this.posZ; if (z < 0){--z;}

        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	worldObj.spawnEntityInWorld(new IceBoulderEntity(worldObj, this.posX - 5 + random.nextInt(10), this.posY + 16 + random.nextInt(5), this.posZ - 5 + random.nextInt(10), 0.0D, -1.0D, 0.0D));
        	
        	this.setDead();
        }
    }
}
