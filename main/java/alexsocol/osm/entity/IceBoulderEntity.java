package alexsocol.osm.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class IceBoulderEntity extends SlowlyMovingUnstopableAbstractEntity {
	
	public int one = 1;
	
	public IceBoulderEntity(World world) {
		super(world);
	}
	
	public IceBoulderEntity(World world, double par2, double par4, double par6, double par8, double par10, double par12) {
		super(world, par2, par4, par6, par8, par10, par12);
		this.shootingEntity = null;
	}
	
	@Override
	protected void onImpact(MovingObjectPosition mop) {
        if (!this.worldObj.isRemote) {
            if (mop.entityHit != null) {
            	mop.entityHit.attackEntityFrom(DamageSource.causeIndirectMagicDamage(this, this.shootingEntity), 6.0F);
            }
            
            for (int i = 0; i < 4; i++){
            	this.worldObj.spawnParticle("cloud", this.posX, this.posY, this.posZ, 0.0, 0.0, 0.0);
            }
            
            this.worldObj.newExplosion((Entity)null, this.posX, this.posY, this.posZ, (float)this.one, true, false);
            this.setDead();
        }
    }
	
	@Override
	public void onUpdate(){
		super.onUpdate();
		for (int i = 0; i < 16; ++i) {
            this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
        }
	}
	
	/**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     *
    public void writeEntityToNBT(NBTTagCompound nbt)
    {
        super.writeEntityToNBT(nbt);
        nbt.setInteger("ExplosionPower", this.one);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     *
    public void readEntityFromNBT(NBTTagCompound nbt)
    {
        super.readEntityFromNBT(nbt);

        if (nbt.hasKey("ExplosionPower", 99))
        {
            this.one = nbt.getInteger("ExplosionPower");
        }
    }*/
}
