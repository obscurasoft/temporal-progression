package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ResonatingObscuraBucket extends Item {
	public ResonatingObscuraBucket() {
		this.setCreativeTab(OSMMain.osmTab);
		this.setMaxStackSize(1);
		this.setUnlocalizedName("ResonatingObscuraBucket");
		this.setTextureName(ModInfo.MODID + ":ResonatingObscuraBucket");
	}
	
	public boolean onItemUse(ItemStack itemstack, EntityPlayer player, World world, int par4, int par5, int par6, int par7, float f8, float f9, float f10) {
        if (par7 == 0) { --par5; }

        if (par7 == 1) { ++par5; }

        if (par7 == 2) { --par6; }

        if (par7 == 3) { ++par6; }

        if (par7 == 4) { --par4; }

        if (par7 == 5) { ++par4; }

        if (!player.canPlayerEdit(par4, par5, par6, par7, itemstack)) { return false; }
        
        else {
            if (world.isAirBlock(par4, par5, par6)) {
                world.setBlock(par4, par5, par6, CommonProxy.obscuraFluid);
                if (!player.capabilities.isCreativeMode){
                	player.inventory.consumeInventoryItem(CommonProxy.resonatingObscuraBucket);
                }
                if (itemstack.stackSize <= 0){
                	player.inventory.addItemStackToInventory(new ItemStack(Items.bucket));
                }
            }
            return true;
        }
    }
}
