package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class ObscuriteChunk extends Item {
	public ObscuriteChunk() {
		this.setUnlocalizedName("ObscuriteChunk");
		this.setTextureName(ModInfo.MODID + ":ObscuriteChunk");
		this.setCreativeTab(OSMMain.osmTab);
	}

}
