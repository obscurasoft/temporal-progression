package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class InactiveResonator extends Item {
	public InactiveResonator() {
		this.setUnlocalizedName("InactiveResonator");
		this.setTextureName(ModInfo.MODID + ":ResonatorInactive");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
