package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class XPStorage extends Item {
	
	public XPStorage(){
		this.setCreativeTab(OSMMain.osmTab);
		this.setMaxDamage(1024);
		this.setMaxStackSize(1);
		this.setTextureName(ModInfo.MODID + ":XPStorage");
		this.setUnlocalizedName("XPStorage");
	}
	
	public boolean isItemStackDamageable(){
		return true;
	}
	
	public boolean isDamageable(){
		return true;
	}
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
	    if (!world.isRemote) {
	        if (!player.isSneaking()) {
	            if (stack.getItemDamage() < stack.getMaxDamage() && canExperience(player, 100)) {
	                doExperience(player, 100);
	                stack.setItemDamage(stack.getItemDamage() + 1);
	            }
	        } else {
	            if (stack.getItemDamage() > 0 && canExperience(player, -100)) {
	                doExperience(player, -100);
	                stack.setItemDamage(stack.getItemDamage() - 1);
	            }
	        }
	    }
	    return stack;
	}

	private static boolean canExperience(EntityPlayer player, int exp) {
	    if (0 > exp && player.experienceTotal + exp >= 0) return true;
	    if (0 < exp && player.experienceTotal + exp < Integer.MAX_VALUE) return true;
	    return false;
	}

	public static void doExperience(EntityPlayer player, int exp) {
	    player.experienceTotal += exp;
	    player.experience += (float)exp / (float)player.xpBarCap();
	    //positive
	    if (exp > 0) {
	        while (player.experience >= 1.0F) {
	            player.experience -= 1.0F;
	            player.addExperienceLevel(1);
	        }
	    }
	    //negative
	    if (exp < 0) {
	        while (player.experience < 0.0F) {
	            player.experience += 1.0F;
	            player.addExperienceLevel(-1);
	        }
	    }
	    //
	}
	public boolean showDurabilityBar(){
        return false;
    }
}