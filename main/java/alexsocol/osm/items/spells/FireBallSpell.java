package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.entity.FireBallEntity;
import alexsocol.osm.entity.IceBallEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class FireBallSpell extends Item {
	
	public FireBallSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":FireBallSpell");
		this.setUnlocalizedName("FireBallSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 56;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	//Next Code from ghast added by r4v3n6101 advice
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	Vec3 vec3 = player.getLook(1.0F);
        	double d8 = 4.0D;
        	world.spawnEntityInWorld(new FireBallEntity(world, player.posX + vec3.xCoord * d8, player.posY, player.posZ + vec3.zCoord * d8, vec3.xCoord, vec3.yCoord, vec3.zCoord));
        }
        return stack;
    }
}