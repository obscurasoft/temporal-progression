package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.entity.IceBallEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ExplosionSpell extends Item {
	
	public ExplosionSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":ExplosionSpell");
		this.setUnlocalizedName("ExplosionSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 80;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	@Override
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {
		if (!world.isRemote){
			MovingObjectPosition mop = block(player, 1.0F, 16.0D, true);
			if (mop != null) {
				world.createExplosion((Entity) null, mop.blockX, mop.blockY, mop.blockZ, 4.0F, false);
			}
		}
		return stack;
	}

	public static MovingObjectPosition block(EntityPlayer player, float fasc, double dist, boolean interact) {
		Vec3 vec3 = player.getPosition(fasc);
		vec3.yCoord += player.getEyeHeight();
		Vec3 vec31 = player.getLook(fasc);
		Vec3 vec32 = vec3.addVector(vec31.xCoord * dist, vec31.yCoord * dist, vec31.zCoord * dist);
		MovingObjectPosition movingobjectposition = player.worldObj.rayTraceBlocks(vec3, vec32, interact);
		return movingobjectposition;
	}
}