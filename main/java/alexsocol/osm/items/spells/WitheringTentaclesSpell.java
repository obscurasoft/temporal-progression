package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class WitheringTentaclesSpell extends Item {
	
	public WitheringTentaclesSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":WitheringTentaclesSpell");
		this.setUnlocalizedName("WitheringTentaclesSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 56;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	world.setBlock((int)player.posX + 1, (int)player.posY, (int)player.posZ, CommonProxy.obscuraCrystal);
        }
        return stack;
    }
}
