package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.entity.FireBallEntity;
import alexsocol.osm.entity.IceBallEntity;
import alexsocol.osm.entity.IceBoulderEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class IceStormSpell extends Item {
	
	public IceStormSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":IceStormSpell");
		this.setUnlocalizedName("IceStormSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 72;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	world.spawnEntityInWorld(new IceBallEntity(world, player));

        	/*Vec3 vec3 = player.getLook(1.0F);
        	double d8 = 4.0D;
        	world.spawnEntityInWorld(new IceBoulderEntity(world, player.posX + vec3.xCoord * d8, player.posY, player.posZ + vec3.zCoord * d8, vec3.xCoord, vec3.yCoord, vec3.zCoord));*/
        }
        return stack;
    }
}
