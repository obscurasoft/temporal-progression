package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.entity.FlameArrowEntity;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class FlameArrowSpell extends Item {

	public FlameArrowSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":FlameArrowSpell");
		this.setUnlocalizedName("FlameArrowSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 40;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	world.spawnEntityInWorld(new FlameArrowEntity(world, player, 2.0F));
        }
        return stack;
	}
}
