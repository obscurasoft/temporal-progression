package alexsocol.osm.items.spells;

import java.util.Random;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;

public class MeteoricStrikeSpell extends Item {
	
	private static Random random = new Random(); 
	
	public MeteoricStrikeSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":MeteoricStrikeSpell");
		this.setUnlocalizedName("MeteoricStrikeSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 108;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	int X = (int)player.posX;
        	int Y = (int)player.posY;
        	int Z = (int)player.posZ;
        	
        	if (player.posX < 0){
        		X = (int)player.posX - 1;
        	}
        	if (player.posZ < 0){
        		Z = (int)player.posZ - 1;
        	}
        	
        	player.addPotionEffect((new PotionEffect(Potion.resistance.id, 200, 64)));
        	
        	for (int l = 0; l < 32; l += 4){
	        	for (int i = 0; i < 32; i++){
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X + 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X - 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z + 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z - 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X + 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z + 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X - 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z - 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X + 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z - 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        		world.spawnEntityInWorld(new EntityLargeFireball(world, X - 10 + random.nextInt(10), Y + l + 16 + random.nextInt(32) + random.nextInt(64), Z + 10 + random.nextInt(10), 0.0D, -3.0D, 0.0D));
	        	}
        	}
        }
        return stack;
    }
}
