package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ForceFieldSpell extends Item {
	
	public ForceFieldSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":ForceFieldSpell");
		this.setUnlocalizedName("ForceFieldSpell");
	}
	
	public int getMaxItemUseDuration(ItemStack stack){
        return 56;
    }
	
	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}
	
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	int X = (int)player.posX;
        	int Y = (int)player.posY;
        	int Z = (int)player.posZ;
        	
        	if (player.posX < 0){
        		X = (int)player.posX - 1;
        	}
        	if (player.posZ < 0){
        		Z = (int)player.posZ - 1;
        	}
        	
        	for (int y = -1; y < 2; y++){
        		for (int z = -1; z < 2; z++){
        			if (world.getBlock(X + 2, Y + y, Z + z) == Blocks.air) {world.setBlock(X + 2, Y + y, Z + z, CommonProxy.forceField);}
        		}
        	}
        	
        	for (int y = -1; y < 2; y++){
        		for (int z = -1; z < 2; z++){
        			if (world.getBlock(X - 2, Y + y, Z + z) == Blocks.air) {world.setBlock(X - 2, Y + y, Z + z, CommonProxy.forceField);}
        		}
        	}
        	
        	for (int x = -1; x < 2; x++){
        		for (int z = -1; z < 2; z++){
        			if (world.getBlock(X + x, Y + 2, Z + z) == Blocks.air) {world.setBlock(X + x, Y + 2, Z + z, CommonProxy.forceField);}
        		}
        	}
        	
        	for (int x = -1; x < 2; x++){
        		for (int z = -1; z < 2; z++){
        			if (world.getBlock(X + x, Y - 2, Z + z) == Blocks.air) {world.setBlock(X + x, Y - 2, Z + z, CommonProxy.forceField);}
        		}
        	}
        	
        	for (int x = -1; x < 2; x++){
        		for (int y = -1; y < 2; y++){
        			if (world.getBlock(X + x, Y + y, Z + 2) == Blocks.air) {world.setBlock(X + x, Y + y, Z + 2, CommonProxy.forceField);}
        		}
        	}
        	
        	for (int x = -1; x < 2; x++){
        		for (int y = -1; y < 2; y++){
        			if (world.getBlock(X + x, Y + y, Z - 2) == Blocks.air) {world.setBlock(X + x, Y + y, Z - 2, CommonProxy.forceField);}
        		}
        	}
        }
        return stack;
    }
}
