package alexsocol.osm.items.spells;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class HealingMundaneSpell extends Item {
	
	public HealingMundaneSpell(){
		this.setCreativeTab(OSMMain.spellTab);
		this.setTextureName(ModInfo.MODID + ":HealingMundaneSpell");
		this.setUnlocalizedName("HealingMundaneSpell");
	}

	public int getMaxItemUseDuration(ItemStack stack){
        return 32;
    }

	public EnumAction getItemUseAction(ItemStack stack){
        return EnumAction.bow;
    }

	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        return stack;
	}

	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player){
        if (!world.isRemote){
        	player.heal(5);
        }
        return stack;
    }
}