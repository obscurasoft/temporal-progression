package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class MisteryCrystalCondenser extends Item {
	
	public MisteryCrystalCondenser() {
		this.setUnlocalizedName("MisteryCrystalCondenser");
		this.setTextureName(ModInfo.MODID + ":MisteryCrystalCondenser");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
