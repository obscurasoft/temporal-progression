package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class MisteryCrystal extends Item {
	
	public MisteryCrystal() {
		this.setUnlocalizedName("MisteryCrystal");
		this.setTextureName(ModInfo.MODID + ":MisteryCrystal");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
