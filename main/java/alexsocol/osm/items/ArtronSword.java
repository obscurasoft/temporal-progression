package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.item.ItemSword;

public class ArtronSword extends ItemSword {
	
	public ArtronSword(){
		super(CommonProxy.obscuraToolMaterial);
		this.setCreativeTab(OSMMain.osmTab);
		this.setTextureName(ModInfo.MODID + ":ArtronSword");
		this.setUnlocalizedName("ArtronSword");
	}
}
