package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class MisteryCrystalCondenserActive extends Item {
	
	public MisteryCrystalCondenserActive() {
		this.setUnlocalizedName("MisteryCrystalCondenserActive");
		this.setTextureName(ModInfo.MODID + ":MisteryCrystalCondenserActive");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
