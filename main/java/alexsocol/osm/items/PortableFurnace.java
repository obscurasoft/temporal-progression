package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.world.World;

public class PortableFurnace extends Item {
	public PortableFurnace(){
		this.setCreativeTab(OSMMain.osmTab);
		this.setTextureName(ModInfo.MODID + ":PortableFurnace");
		this.setUnlocalizedName("PortableFurnace");
	}
	
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		return stack;
	}
}
