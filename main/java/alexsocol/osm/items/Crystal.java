package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class Crystal extends Item {
	
	public Crystal() {
		this.setCreativeTab(OSMMain.osmTab);
		this.setUnlocalizedName("Crystal");
		this.setTextureName(ModInfo.MODID + ":Crystal");
	}
}
