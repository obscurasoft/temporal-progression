package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.ItemFood;

public class UltraCheese extends ItemFood {
	
	public UltraCheese(){
		super(5, true);
		this.setCreativeTab(OSMMain.osmTab);
		this.setTextureName(ModInfo.MODID + ":UltraCheese");
		this.setUnlocalizedName("UltraCheese");
	}
}
