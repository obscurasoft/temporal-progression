package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class Catcher extends Item {
	
	public Catcher() {
		this.setCreativeTab(OSMMain.osmTab);
		this.setUnlocalizedName("Catcher");
		this.setTextureName(ModInfo.MODID + ":Catcher");
	}
}
