package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class DarkDust extends Item {
	public DarkDust() {
		this.setUnlocalizedName("DarkDust");
		this.setTextureName(ModInfo.MODID + ":DarkDust");
		this.setCreativeTab(OSMMain.osmTab);
	}
}