package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Resonator extends Item {
	
	public Resonator() {
		super();
		this.doesContainerItemLeaveCraftingGrid(new ItemStack(CommonProxy.inactiveResonator));
		this.setMaxStackSize(1);
		this.setUnlocalizedName("Resonator");
		this.setTextureName(ModInfo.MODID + ":ResonatorActive");
		this.setCreativeTab(OSMMain.osmTab);
	}

	private static String KEY_PAIRLOC = "PAIRLOC";
	private static String HAB_PAIRLOC = "HABLOC";
	private static String KEEP_BINDING = "KEEPBINDING";
	private static String MODE = "WRENCHMODE";

	private static final int MODE_PAIR = 0;
	private static final int MODE_DISCONNECT = 1;

	@Override
	public boolean getShareTag(){
		return true;
	}

	@Override
	public int getItemStackLimit(){
		return 1;
	}

}