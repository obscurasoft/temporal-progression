package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.item.ItemSword;

public class Sabre extends ItemSword {
	
	public Sabre(){
		super(CommonProxy.obscuraToolMaterial);
		this.setCreativeTab(OSMMain.osmTab);
		this.setTextureName(ModInfo.MODID + ":Sabre");
		this.setUnlocalizedName("Sabre");
	}
}
