package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class BrassIngot extends Item {
	
	public BrassIngot() {
		this.setCreativeTab(OSMMain.osmTab);
		this.setUnlocalizedName("BrassIngot");
		this.setTextureName(ModInfo.MODID + ":BrassIngot");
	}
}
