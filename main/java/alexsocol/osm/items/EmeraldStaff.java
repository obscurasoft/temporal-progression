package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class EmeraldStaff extends Item {
	public EmeraldStaff() {
		this.setUnlocalizedName("EmeraldStaff");
		this.setTextureName(ModInfo.MODID + ":EmeraldStaff");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
