package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class StrangeMatter extends Item {
	public StrangeMatter() {
		this.setUnlocalizedName("StrangeMatter");
		this.setTextureName(ModInfo.MODID + ":StrangeMatter");
		this.setCreativeTab(OSMMain.osmTab);
	}
}
