package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class Stasis extends ItemFood {
	
	public Stasis() {
		super(10, true);
		this.setCreativeTab(OSMMain.osmTab);
		this.setUnlocalizedName("Stasis");
		this.setTextureName(ModInfo.MODID + ":Stasis");
	}
	
	public EnumAction getItemUseAction(ItemStack stack) {
        return EnumAction.drink;
    }
	
	public int getMaxItemUseDuration(ItemStack stack) {
        return 72;
    }
}
