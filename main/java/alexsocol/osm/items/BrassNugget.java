package alexsocol.osm.items;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.item.Item;

public class BrassNugget extends Item {
	
	public BrassNugget() {
		this.setCreativeTab(OSMMain.osmTab);
		this.setUnlocalizedName("BrassNugget");
		this.setTextureName(ModInfo.MODID + ":BrassNugget");
	}
}
