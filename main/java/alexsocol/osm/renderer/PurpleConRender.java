package alexsocol.osm.renderer;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import alexsocol.osm.ModInfo;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;

public class PurpleConRender extends TileEntitySpecialRenderer {

	private PurpleConModel model;
	
    private static final ResourceLocation beam = new ResourceLocation(ModInfo.MODID + ":textures/entity/PurpleConBeam.png");

	public PurpleConRender() {
		model = new PurpleConModel();
	}

	@Override
	public void renderTileEntityAt(TileEntity te, double dx, double dy, double dz, float f) {
		renderTE((PurpleConTileEntity)te, dx, dy, dz, f);
		
		GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F); 

		Tessellator tessellator = Tessellator.instance; 
		this.bindTexture(beam); 
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, 10497.0F); 
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, 10497.0F); 
		GL11.glDisable(GL11.GL_LIGHTING); 
		GL11.glDisable(GL11.GL_CULL_FACE); 
		GL11.glDisable(GL11.GL_BLEND); 
		GL11.glDepthMask(true); 
		OpenGlHelper.glBlendFunc(770, 1, 1, 0); 
		float f2 = (float)te.getWorldObj().getTotalWorldTime() + f; 
		float f3 = -f2 * 0.2F - (float)MathHelper.floor_float(-f2 * 0.1F); 
		byte b0 = 1; 
		double d3 = (double)f2 * 0.025D * (1.0D - (double)(b0 & 1) * 2.5D); 
		tessellator.startDrawingQuads(); 
		tessellator.setColorRGBA(255, 255, 255, 32); 
		double d5 = (double)b0 * 0.2D; 
		double d7 = 0.5D + Math.cos(d3 + 2.356194490192345D) * d5; 
		double d9 = 0.5D + Math.sin(d3 + 2.356194490192345D) * d5; 
		double d11 = 0.5D + Math.cos(d3 + (Math.PI / 4D)) * d5; 
		double d13 = 0.5D + Math.sin(d3 + (Math.PI / 4D)) * d5; 
		double d15 = 0.5D + Math.cos(d3 + 3.9269908169872414D) * d5; 
		double d17 = 0.5D + Math.sin(d3 + 3.9269908169872414D) * d5; 
		double d19 = 0.5D + Math.cos(d3 + 5.497787143782138D) * d5; 
		double d21 = 0.5D + Math.sin(d3 + 5.497787143782138D) * d5; 
		double d23 = (double)(256.0F * 1.0F); 
		double d25 = 0.0D; 
		double d27 = 1.0D; 
		double d28 = (double)(-1.0F + f3); 
		double d29 = (double)(256.0F * 1.0F) * (0.5D / d5) + d28; 
		tessellator.addVertexWithUV(dx + d7, dy + d23, dz + d9, d27, d29); 
		tessellator.addVertexWithUV(dx + d7, dy, dz + d9, d27, d28); 
		tessellator.addVertexWithUV(dx + d11, dy, dz + d13, d25, d28); 
		tessellator.addVertexWithUV(dx + d11, dy + d23, dz + d13, d25, d29); 
		tessellator.addVertexWithUV(dx + d19, dy + d23, dz + d21, d27, d29); 
		tessellator.addVertexWithUV(dx + d19, dy, dz + d21, d27, d28); 
		tessellator.addVertexWithUV(dx + d15, dy, dz + d17, d25, d28); 
		tessellator.addVertexWithUV(dx + d15, dy + d23, dz + d17, d25, d29); 
		tessellator.addVertexWithUV(dx + d11, dy + d23, dz + d13, d27, d29); 
		tessellator.addVertexWithUV(dx + d11, dy, dz + d13, d27, d28); 
		tessellator.addVertexWithUV(dx + d19, dy, dz + d21, d25, d28); 
		tessellator.addVertexWithUV(dx + d19, dy + d23, dz + d21, d25, d29); 
		tessellator.addVertexWithUV(dx + d15, dy + d23, dz + d17, d27, d29); 
		tessellator.addVertexWithUV(dx + d15, dy, dz + d17, d27, d28); 
		tessellator.addVertexWithUV(dx + d7, dy, dz + d9, d25, d28); 
		tessellator.addVertexWithUV(dx + d7, dy + d23, dz + d9, d25, d29); 
		tessellator.draw(); 
		GL11.glEnable(GL11.GL_BLEND); 
		OpenGlHelper.glBlendFunc(770, 771, 1, 0); 
		GL11.glDepthMask(false); 
		tessellator.startDrawingQuads(); 
		tessellator.setColorRGBA(255, 255, 255, 32); 
		double d30 = 0.2D; 
		double d4 = 0.2D; 
		double d6 = 0.8D; 
		double d8 = 0.2D; 
		double d10 = 0.2D; 
		double d12 = 0.8D; 
		double d14 = 0.8D; 
		double d16 = 0.8D; 
		double d18 = (double)(256.0F * 1.0F); 
		double d20 = 0.0D; 
		double d22 = 1.0D; 
		double d24 = (double)(-1.0F + f3); 
		double d26 = (double)(256.0F * 1.0F) + d24; 
		tessellator.addVertexWithUV(dx + d30, dy + d18, dz + d4, d22, d26); 
		tessellator.addVertexWithUV(dx + d30, dy, dz + d4, d22, d24); 
		tessellator.addVertexWithUV(dx + d6, dy, dz + d8, d20, d24); 
		tessellator.addVertexWithUV(dx + d6, dy + d18, dz + d8, d20, d26); 
		tessellator.addVertexWithUV(dx + d14, dy + d18, dz + d16, d22, d26); 
		tessellator.addVertexWithUV(dx + d14, dy, dz + d16, d22, d24); 
		tessellator.addVertexWithUV(dx + d10, dy, dz + d12, d20, d24); 
		tessellator.addVertexWithUV(dx + d10, dy + d18, dz + d12, d20, d26); 
		tessellator.addVertexWithUV(dx + d6, dy + d18, dz + d8, d22, d26); 
		tessellator.addVertexWithUV(dx + d6, dy, dz + d8, d22, d24); 
		tessellator.addVertexWithUV(dx + d14, dy, dz + d16, d20, d24); 
		tessellator.addVertexWithUV(dx + d14, dy + d18, dz + d16, d20, d26); 
		tessellator.addVertexWithUV(dx + d10, dy + d18, dz + d12, d22, d26); 
		tessellator.addVertexWithUV(dx +
		d10, dy, dz + d12, d22, d24); 
		tessellator.addVertexWithUV(dx + d30, dy, dz + d4, d20, d24); 
		tessellator.addVertexWithUV(dx + d30, dy + d18, dz + d4, d20, d26); 
		tessellator.draw(); 
		GL11.glEnable(GL11.GL_LIGHTING); 
		GL11.glEnable(GL11.GL_TEXTURE_2D); 
		GL11.glDepthMask(true);
	}

	private void renderTE(PurpleConTileEntity te, double dx, double dy, double dz, float f) {

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glTranslated(dx, dy, dz);
		GL11.glTranslatef(0.5F, 1.5F, 0.5F);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		bindTexture(new ResourceLocation(ModInfo.MODID + ":textures/blocks/PurpleCon.png"));
		model.render_part1(0.0625F);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glTranslated(dx, dy, dz);
		GL11.glTranslatef(0.5F, 1.5F, 0.5F);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		if (te != null) {
			GL11.glRotatef(0F + te.rotateF, 0.0F, 1.0F, 0.0F);
			//GL11.glRotatef(0F + te.rotateF, 0.0F, 0.0F, 1.0F);
		}
		bindTexture(new ResourceLocation(ModInfo.MODID + ":textures/blocks/PurpleCon.png"));
		model.render_part2(0.0625F);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();

	}

}