package alexsocol.osm.renderer;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import alexsocol.osm.ModInfo;
import alexsocol.osm.blocks.tileentity.AbstractEffectTileEntity;

public class AbstractEffectRender extends TileEntitySpecialRenderer {

	private AbstractEffectModel model;
	
    private static final ResourceLocation beam = new ResourceLocation(ModInfo.MODID + ":textures/entity/AbstractEffectBeam.png");

	public AbstractEffectRender() {
		model = new AbstractEffectModel();
	}

	@Override
	public void renderTileEntityAt(TileEntity te, double dx, double dy, double dz, float f) {
		renderTE((AbstractEffectTileEntity)te, dx, dy, dz, f);
	}

	private void renderTE(AbstractEffectTileEntity te, double dx, double dy, double dz, float f) {
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glTranslated(dx, dy, dz);
		GL11.glTranslatef(0.5F, 1.5F, 0.5F);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		if (te != null) {
			GL11.glRotatef(0F + te.rotateF, 0.0F, 1.0F, 0.0F);
			//GL11.glRotatef(0F + te.rotateF, 0.0F, 0.0F, 1.0F);
		}
		bindTexture(new ResourceLocation(ModInfo.MODID + ":textures/blocks/AbstractEffect.png"));
		model.render_part2(0.0625F);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();

	}

}