package alexsocol.osm.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class AbstractEffectModel extends ModelBase {

	ModelRenderer shape1;
	ModelRenderer shape2;
	ModelRenderer shape3;
	ModelRenderer shape4;
	ModelRenderer shape5;
	ModelRenderer shape6;
	ModelRenderer shape7;
	ModelRenderer shape8;
	
	public AbstractEffectModel() {
		textureWidth = 56;
		textureHeight = 64;

		shape1 = new ModelRenderer(this, 0, 0);
		shape1.addBox(-3F, -6F, 19F, 6, 12, 1);
		shape1.setRotationPoint(0F, 16F, 0F);
		shape1.setTextureSize(56, 64);
		shape1.mirror = true;
		setRotation(shape1, 0F, 0F, 0F);
		shape2 = new ModelRenderer(this, 14, 0);
		shape2.addBox(-3F, -6F, -20F, 6, 12, 1);
		shape2.setRotationPoint(0F, 16F, 0F);
		shape2.setTextureSize(56, 64);
		shape2.mirror = true;
		setRotation(shape2, 0F, 0F, 0F);
		shape3 = new ModelRenderer(this, 28, 0);
		shape3.addBox(-3F, -6F, -20F, 6, 12, 1);
		shape3.setRotationPoint(0F, 16F, 0F);
		shape3.setTextureSize(56, 64);
		shape3.mirror = true;
		setRotation(shape3, 0F, 1.570796F, 0F);
		shape4 = new ModelRenderer(this, 42, 0);
		shape4.addBox(-3F, -6F, 19F, 6, 12, 1);
		shape4.setRotationPoint(0F, 16F, 0F);
		shape4.setTextureSize(56, 64);
		shape4.mirror = true;
		setRotation(shape4, 0F, 1.570796F, 0F);
		
		shape5 = new ModelRenderer(this, 0, 13);
		shape5.addBox(-3F, -6F, 19F, 6, 12, 1);
		shape5.setRotationPoint(0F, 16F, 0F);
		shape5.setTextureSize(56, 64);
		shape5.mirror = true;
		setRotation(shape5, 0F, 0.785398F, 0F);
		shape6 = new ModelRenderer(this, 14, 13);
		shape6.addBox(-3F, -6F, -20F, 6, 12, 1);
		shape6.setRotationPoint(0F, 16F, 0F);
		shape6.setTextureSize(56, 64);
		shape6.mirror = true;
		setRotation(shape6, 0F, 0.785398F, 0F);
		shape7 = new ModelRenderer(this, 28, 13);
		shape7.addBox(-3F, -6F, -20F, 6, 12, 1);
		shape7.setRotationPoint(0F, 16F, 0F);
		shape7.setTextureSize(56, 64);
		shape7.mirror = true;
		setRotation(shape7, 0F, -0.785398F, 0F);
		shape8 = new ModelRenderer(this, 42, 13);
		shape8.addBox(-3F, -6F, 19F, 6, 12, 1);
		shape8.setRotationPoint(0F, 16F, 0F);
		shape8.setTextureSize(56, 64);
		shape8.mirror = true;
		setRotation(shape8, 0F, -0.785398F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		render(f5);
	}

	public void render(float f5) {
		shape1.render(f5);
		shape2.render(f5);
		shape3.render(f5);
		shape4.render(f5);
		shape5.render(f5);
		shape6.render(f5);
		shape7.render(f5);
		shape8.render(f5);
	}

	public void render_part2(float f5) {
		shape1.render(f5);
		shape2.render(f5);
		shape3.render(f5);
		shape4.render(f5);
		shape5.render(f5);
		shape6.render(f5);
		shape7.render(f5);
		shape8.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
