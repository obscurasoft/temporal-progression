package alexsocol.osm.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ObscuraCrystalModel extends ModelBase
{
  //fields
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape7;
    ModelRenderer Shape8;
    ModelRenderer Shape9;
    ModelRenderer Shape10;
    ModelRenderer Shape11;
    ModelRenderer Shape12;
  
  public ObscuraCrystalModel()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      Shape1 = new ModelRenderer(this, 25, 19);
      Shape1.addBox(0F, 0F, 0F, 3, 6, 3);
      Shape1.setRotationPoint(-1F, 24F, 0F);
      Shape1.setTextureSize(64, 32);
      Shape1.mirror = true;
      setRotation(Shape1, 3.141593F, 2.949606F, 0.8210277F);
      Shape2 = new ModelRenderer(this, 56, 0);
      Shape2.addBox(0F, 0F, 0F, 2, 8, 2);
      Shape2.setRotationPoint(3F, 24F, 0F);
      Shape2.setTextureSize(64, 32);
      Shape2.mirror = true;
      setRotation(Shape2, -2.918521F, -1.163897F, 0.4066927F);
      Shape3 = new ModelRenderer(this, 0, 0);
      Shape3.addBox(0F, 0F, 0F, 4, 12, 4);
      Shape3.setRotationPoint(0F, 24F, 0F);
      Shape3.setTextureSize(64, 32);
      Shape3.mirror = true;
      setRotation(Shape3, 3.141593F, -1.317982F, 0.4712389F);
      Shape4 = new ModelRenderer(this, 16, 0);
      Shape4.addBox(0F, 0F, 0F, 4, 16, 4);
      Shape4.setRotationPoint(0F, 24F, 0F);
      Shape4.setTextureSize(64, 32);
      Shape4.mirror = true;
      setRotation(Shape4, -2.959521F, 1.660954F, 0.2617994F);
      Shape5 = new ModelRenderer(this, 0, 20);
      Shape5.addBox(0F, 0F, 0F, 3, 9, 3);
      Shape5.setRotationPoint(-1F, 24F, -3F);
      Shape5.setTextureSize(64, 32);
      Shape5.mirror = true;
      setRotation(Shape5, -2.881342F, -0.122173F, 0.0523599F);
      Shape6 = new ModelRenderer(this, 32, 0);
      Shape6.addBox(0F, 0F, 0F, 4, 15, 4);
      Shape6.setRotationPoint(-3F, 24F, 2F);
      Shape6.setTextureSize(64, 32);
      Shape6.mirror = true;
      setRotation(Shape6, 2.792527F, -0.6457718F, -0.2443461F);
      Shape7 = new ModelRenderer(this, 48, 0);
      Shape7.addBox(0F, 0F, 0F, 2, 7, 2);
      Shape7.setRotationPoint(-1F, 24F, 2F);
      Shape7.setTextureSize(64, 32);
      Shape7.mirror = true;
      setRotation(Shape7, -2.614276F, 2.872254F, 0.426418F);
      Shape8 = new ModelRenderer(this, 37, 19);
      Shape8.addBox(0F, 0F, 0F, 3, 8, 3);
      Shape8.setRotationPoint(1F, 24F, 2F);
      Shape8.setTextureSize(64, 32);
      Shape8.mirror = true;
      setRotation(Shape8, -2.544463F, -2.920483F, 0.2344318F);
      Shape9 = new ModelRenderer(this, 16, 0);
      Shape9.addBox(0F, 0F, 0F, 4, 15, 4);
      Shape9.setRotationPoint(0F, 24F, 0F);
      Shape9.setTextureSize(64, 32);
      Shape9.mirror = true;
      setRotation(Shape9, 3.001966F, 0.1745329F, 0.122173F);
      Shape10 = new ModelRenderer(this, 13, 19);
      Shape10.addBox(0F, 0F, 0F, 3, 10, 3);
      Shape10.setRotationPoint(2F, 24F, -4F);
      Shape10.setTextureSize(64, 32);
      Shape10.mirror = true;
      setRotation(Shape10, -2.918521F, -0.2981518F, 0.1927091F);
      Shape11 = new ModelRenderer(this, 25, 19);
      Shape11.addBox(0F, 0F, 0F, 3, 6, 3);
      Shape11.setRotationPoint(3F, 24F, -1F);
      Shape11.setTextureSize(64, 32);
      Shape11.mirror = true;
      setRotation(Shape11, -2.918521F, -0.4203249F, 0.3323355F);
      Shape12 = new ModelRenderer(this, 25, 19);
      Shape12.addBox(0F, 0F, 0F, 3, 6, 3);
      Shape12.setRotationPoint(-1F, 24F, -3F);
      Shape12.setTextureSize(64, 32);
      Shape12.mirror = true;
      setRotation(Shape12, -2.726535F, 1.244451F, 0.3672421F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Shape1.render(f5);
    Shape2.render(f5);
    Shape3.render(f5);
    Shape4.render(f5);
    Shape5.render(f5);
    Shape6.render(f5);
    Shape7.render(f5);
    Shape8.render(f5);
    Shape9.render(f5);
    Shape10.render(f5);
    Shape11.render(f5);
    Shape12.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
