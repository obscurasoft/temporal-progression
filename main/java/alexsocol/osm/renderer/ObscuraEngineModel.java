package alexsocol.osm.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ObscuraEngineModel extends ModelBase {
	// fields
	ModelRenderer Base;
	ModelRenderer Midle2;
	ModelRenderer Cone;
	ModelRenderer Midle1;

	public ObscuraEngineModel() {
		textureWidth = 64;
		textureHeight = 32;

		Base = new ModelRenderer(this, 0, 0);
		Base.addBox(0F, 0F, 0F, 10, 9, 10);
		Base.setRotationPoint(-5F, 15F, -5F);
		Base.setTextureSize(64, 32);
		Base.mirror = true;
		setRotation(Base, 0F, 0F, 0F);
		Midle2 = new ModelRenderer(this, 0, 21);
		Midle2.addBox(0F, 0F, 0F, 2, 4, 2);
		Midle2.setRotationPoint(-0.9666666F, 8F, -1F);
		Midle2.setTextureSize(64, 32);
		Midle2.mirror = true;
		setRotation(Midle2, 0F, 0F, 0F);
		Cone = new ModelRenderer(this, 0, 28);
		Cone.addBox(-1F, -1F, -1F, 2, 2, 2);
		Cone.setRotationPoint(0F, 6F, 0F);
		Cone.setTextureSize(64, 32);
		Cone.mirror = true;
		setRotation(Cone, 0F, 0F, 0F);
		Midle1 = new ModelRenderer(this, 9, 23);
		Midle1.addBox(0F, 0F, 0F, 6, 3, 6);
		Midle1.setRotationPoint(-3F, 12F, -3F);
		Midle1.setTextureSize(64, 32);
		Midle1.mirror = true;
		setRotation(Midle1, 0F, 0F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Base.render(f5);
		Midle2.render(f5);
		Cone.render(f5);
		Midle1.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
