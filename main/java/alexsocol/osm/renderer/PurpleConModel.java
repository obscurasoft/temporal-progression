package alexsocol.osm.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class PurpleConModel extends ModelBase {

	ModelRenderer shape1;
	ModelRenderer shape2;
	ModelRenderer shape3;
	ModelRenderer shape4;
	ModelRenderer shape5;
	ModelRenderer shape6;
	ModelRenderer shape7;
	ModelRenderer shape8;
	ModelRenderer shape9;
	ModelRenderer shape10;
	ModelRenderer shape11;
	ModelRenderer shape12;
	ModelRenderer shape13;
	ModelRenderer shape14;
	ModelRenderer shape15;
	ModelRenderer shape16;
	ModelRenderer shape17;

	public PurpleConModel() {
		textureWidth = 64;
		textureHeight = 37;

		shape1 = new ModelRenderer(this, 0, 0);
		shape1.addBox(-8F, -8F, 7.999F, 16, 16, 0);
		shape1.setRotationPoint(0F, 16F, 0F);
		shape1.setTextureSize(64, 37);
		shape1.mirror = true;
		setRotation(shape1, 1.570796F, 0F, 0F);
		shape2 = new ModelRenderer(this, 0, 0);
		shape2.addBox(-8F, -8F, -7.999F, 16, 16, 0);
		shape2.setRotationPoint(0F, 16F, 0F);
		shape2.setTextureSize(64, 37);
		shape2.mirror = true;
		setRotation(shape2, 1.570796F, 0F, 0F);
		shape3 = new ModelRenderer(this, 0, 0);
		shape3.addBox(-8F, -8F, -7.999F, 16, 16, 0);
		shape3.setRotationPoint(0F, 16F, 0F);
		shape3.setTextureSize(64, 37);
		shape3.mirror = true;
		setRotation(shape3, 0F, 1.570796F, 0F);
		shape4 = new ModelRenderer(this, 0, 0);
		shape4.addBox(-8F, -8F, 7.999F, 16, 16, 0);
		shape4.setRotationPoint(0F, 16F, 0F);
		shape4.setTextureSize(64, 37);
		shape4.mirror = true;
		setRotation(shape4, 0F, 1.570796F, 0F);
		shape5 = new ModelRenderer(this, 0, 0);
		shape5.addBox(-8F, -8F, -7.999F, 16, 16, 0);
		shape5.setRotationPoint(0F, 16F, 0F);
		shape5.setTextureSize(64, 37);
		shape5.mirror = true;
		setRotation(shape5, 0F, 0F, 0F);
		shape6 = new ModelRenderer(this, 0, 0);
		shape6.addBox(-8F, -8F, 7.999F, 16, 16, 0);
		shape6.setRotationPoint(0F, 16F, 0F);
		shape6.setTextureSize(64, 37);
		shape6.mirror = true;
		setRotation(shape6, 0F, 0F, 0F);
		shape7 = new ModelRenderer(this, 0, 17);
		shape7.addBox(-7F, -7F, -8F, 14, 14, 1);
		shape7.setRotationPoint(0F, 16F, 0F);
		shape7.setTextureSize(64, 37);
		shape7.mirror = true;
		setRotation(shape7, 1.570796F, 0F, 0F);
		shape8 = new ModelRenderer(this, 0, 19);
		shape8.addBox(-6F, -6F, -7.5F, 12, 12, 1);
		shape8.setRotationPoint(0F, 16F, 0F);
		shape8.setTextureSize(64, 37);
		shape8.mirror = true;
		setRotation(shape8, 1.570796F, 0F, 0F);
		shape9 = new ModelRenderer(this, 0, 21);
		shape9.addBox(-5F, -5F, -7F, 10, 10, 1);
		shape9.setRotationPoint(0F, 16F, 0F);
		shape9.setTextureSize(64, 37);
		shape9.mirror = true;
		setRotation(shape9, 1.570796F, 0F, 0F);
		shape10 = new ModelRenderer(this, 0, 23);
		shape10.addBox(-4F, -4F, -6.5F, 8, 8, 1);
		shape10.setRotationPoint(0F, 16F, 0F);
		shape10.setTextureSize(64, 37);
		shape10.mirror = true;
		setRotation(shape10, 1.570796F, 0F, 0F);
		shape11 = new ModelRenderer(this, 32, 0);
		shape11.addBox(-4F, -4F, -4F, 8, 8, 8);
		shape11.setRotationPoint(0F, 16F, 0F);
		shape11.setTextureSize(64, 37);
		shape11.mirror = true;
		setRotation(shape11, 0F, 0F, 0F);
		shape12 = new ModelRenderer(this, 32, 16);
		shape12.addBox(-3F, -3F, -5F, 6, 6, 1);
		shape12.setRotationPoint(0F, 16F, 0F);
		shape12.setTextureSize(64, 37);
		shape12.mirror = true;
		setRotation(shape12, 1.570796F, 0F, 0F);
		shape13 = new ModelRenderer(this, 46, 16);
		shape13.addBox(-3F, -3F, 4F, 6, 6, 1);
		shape13.setRotationPoint(0F, 16F, 0F);
		shape13.setTextureSize(64, 37);
		shape13.mirror = true;
		setRotation(shape13, 1.570796F, 0F, 0F);
		shape14 = new ModelRenderer(this, 32, 23);
		shape14.addBox(-3F, -3F, 4F, 6, 6, 1);
		shape14.setRotationPoint(0F, 16F, 0F);
		shape14.setTextureSize(64, 37);
		shape14.mirror = true;
		setRotation(shape14, 0F, 0F, 0F);
		shape15 = new ModelRenderer(this, 46, 23);
		shape15.addBox(-3F, -3F, -5F, 6, 6, 1);
		shape15.setRotationPoint(0F, 16F, 0F);
		shape15.setTextureSize(64, 37);
		shape15.mirror = true;
		setRotation(shape15, 0F, 0F, 0F);
		shape16 = new ModelRenderer(this, 32, 30);
		shape16.addBox(-3F, -3F, -5F, 6, 6, 1);
		shape16.setRotationPoint(0F, 16F, 0F);
		shape16.setTextureSize(64, 37);
		shape16.mirror = true;
		setRotation(shape16, 0F, 1.570796F, 0F);
		shape17 = new ModelRenderer(this, 46, 30);
		shape17.addBox(-3F, -3F, 4F, 6, 6, 1);
		shape17.setRotationPoint(0F, 16F, 0F);
		shape17.setTextureSize(64, 37);
		shape17.mirror = true;
		setRotation(shape17, 0F, 1.570796F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		render(f5);
	}

	public void render(float f5) {
		shape1.render(f5);
		shape2.render(f5);
		shape3.render(f5);
		shape4.render(f5);
		shape5.render(f5);
		shape6.render(f5);
		shape7.render(f5);
		shape8.render(f5);
		shape9.render(f5);
		shape10.render(f5);
		shape11.render(f5);
		shape12.render(f5);
		shape13.render(f5);
		shape14.render(f5);
		shape15.render(f5);
		shape16.render(f5);
		shape17.render(f5);
	}

	public void render_part1(float f5) {
		shape1.render(f5);
		shape2.render(f5);
		shape3.render(f5);
		shape4.render(f5);
		shape5.render(f5);
		shape6.render(f5);
		shape7.render(f5);
		shape8.render(f5);
		shape9.render(f5);
		shape10.render(f5);
	}

	public void render_part2(float f5) {
		shape11.render(f5);
		shape12.render(f5);
		shape13.render(f5);
		shape14.render(f5);
		shape15.render(f5);
		shape16.render(f5);
		shape17.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
