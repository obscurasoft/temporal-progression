package alexsocol.osm.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class RitualStoneModel extends ModelBase
{
  //fields
    ModelRenderer Base;
    ModelRenderer angle1;
    ModelRenderer angle2;
    ModelRenderer angle3;
    ModelRenderer angle4;
    ModelRenderer angle5;
    ModelRenderer angle6;
    ModelRenderer angle7;
    ModelRenderer angle8;
  
  public RitualStoneModel()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      Base = new ModelRenderer(this, 0, 4);
      Base.addBox(0F, 0F, 0F, 8, 8, 8);
      Base.setRotationPoint(-4F, 12F, -4F);
      Base.setTextureSize(64, 32);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
      angle1 = new ModelRenderer(this, 0, 0);
      angle1.addBox(0F, 0F, 0F, 2, 2, 2);
      angle1.setRotationPoint(-5F, 19F, -5F);
      angle1.setTextureSize(64, 32);
      angle1.mirror = true;
      setRotation(angle1, 0F, 0F, 0F);
      angle2 = new ModelRenderer(this, 0, 0);
      angle2.addBox(0F, 0F, 0F, 2, 2, 2);
      angle2.setRotationPoint(-5F, 19F, 3F);
      angle2.setTextureSize(64, 32);
      angle2.mirror = true;
      setRotation(angle2, 0F, 0F, 0F);
      angle3 = new ModelRenderer(this, 0, 0);
      angle3.addBox(0F, 0F, 0F, 2, 2, 2);
      angle3.setRotationPoint(3F, 19F, -5F);
      angle3.setTextureSize(64, 32);
      angle3.mirror = true;
      setRotation(angle3, 0F, 0F, 0F);
      angle4 = new ModelRenderer(this, 0, 0);
      angle4.addBox(0F, 0F, 0F, 2, 2, 2);
      angle4.setRotationPoint(-5F, 11F, 3F);
      angle4.setTextureSize(64, 32);
      angle4.mirror = true;
      setRotation(angle4, 0F, 0F, 0F);
      angle5 = new ModelRenderer(this, 0, 0);
      angle5.addBox(0F, 0F, 0F, 2, 2, 2);
      angle5.setRotationPoint(3F, 19F, 3F);
      angle5.setTextureSize(64, 32);
      angle5.mirror = true;
      setRotation(angle5, 0F, 0F, 0F);
      angle6 = new ModelRenderer(this, 0, 0);
      angle6.addBox(0F, 0F, 0F, 2, 2, 2);
      angle6.setRotationPoint(3F, 11F, -5F);
      angle6.setTextureSize(64, 32);
      angle6.mirror = true;
      setRotation(angle6, 0F, 0F, 0F);
      angle7 = new ModelRenderer(this, 0, 0);
      angle7.addBox(0F, 0F, 0F, 2, 2, 2);
      angle7.setRotationPoint(3F, 11F, 3F);
      angle7.setTextureSize(64, 32);
      angle7.mirror = true;
      setRotation(angle7, 0F, 0F, 0F);
      angle8 = new ModelRenderer(this, 0, 0);
      angle8.addBox(0F, 0F, 0F, 2, 2, 2);
      angle8.setRotationPoint(-5F, 11F, -5F);
      angle8.setTextureSize(64, 32);
      angle8.mirror = true;
      setRotation(angle8, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Base.render(f5);
    angle1.render(f5);
    angle2.render(f5);
    angle3.render(f5);
    angle4.render(f5);
    angle5.render(f5);
    angle6.render(f5);
    angle7.render(f5);
    angle8.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
