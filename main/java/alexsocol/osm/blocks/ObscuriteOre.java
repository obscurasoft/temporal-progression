package alexsocol.osm.blocks;

import java.util.Random;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class ObscuriteOre extends Block {

	public ObscuriteOre() {
		super(Material.rock);
		this.setBlockName("ObscuriteOre");
		this.setBlockTextureName(ModInfo.MODID + ":ObscuriteOre");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.5F);
		this.setHarvestLevel("pickaxe", 1);
		this.setResistance(600.F);
		this.setStepSound(soundTypeStone);
	}
	
	@Override
	public Item getItemDropped(int metadata, Random rand, int fortune) {
		return CommonProxy.obscuriteChunk;
	}
	
	@Override
	public int quantityDropped(Random rand) {
		return 1 + rand.nextInt(6);
	}
	
}
