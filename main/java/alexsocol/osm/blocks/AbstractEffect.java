package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.AbstractEffectTileEntity;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class AbstractEffect extends BlockContainer {
	
	public AbstractEffect() {
		super(Material.cake);
		this.setBlockName("AbstractEffect");
		this.setBlockTextureName(ModInfo.MODID + ":AbstractEffectIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(15.0F);
		this.setLightOpacity(1);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeCloth);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new AbstractEffectTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
