package alexsocol.osm.blocks;

import java.util.Random;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlackFlame extends Block {
	
	private IIcon[] icons;
	
	public BlackFlame() {
		super(Material.rock);
		this.setBlockName("BlackFlame");
		this.setBlockTextureName(ModInfo.MODID + ":BlackFlame");
		this.setCreativeTab(OSMMain.osmTab);
		this.setLightLevel(15.0F);
		this.setLightOpacity(0);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
		return null;
	}

	@Override
	public boolean isOpaqueCube() {
        return false;
    }

	@Override
	public boolean renderAsNormalBlock() {
        return false;
    }
	
	/*@Override
	public int getRenderType() {
        return 3;
    }*/

	@Override
	public int quantityDropped(Random rnd) {
        return 0;
    }

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z) {
        return World.doesBlockHaveSolidTopSurface(world, x, y - 1, z);
    }

	@SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister reg) {
        this.icons = new IIcon[] {reg.registerIcon(this.getTextureName() + "_0"), reg.registerIcon(this.getTextureName() + "_1")};
    }

	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int i) {
        return this.icons[i];
    }

	@Override
    public MapColor getMapColor(int i) {
        return MapColor.tntColor;
    }
	
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		entity.setFire(15);
		((EntityLiving) entity).addPotionEffect(new PotionEffect(Potion.blindness.id, 50, 1));       
		((EntityLiving) entity).addPotionEffect(new PotionEffect(Potion.wither.id, 50, 1));       
	}
}
