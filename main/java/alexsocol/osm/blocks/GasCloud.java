package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.GasCloudTileEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Facing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class GasCloud extends BlockContainer {

	private boolean something;
	
	public GasCloud() {
		super(Material.cake);
		this.setBlockName("GasCloud");
		this.setBlockTextureName(ModInfo.MODID + ":GasCloud");
		this.setBlockUnbreakable();
		this.setCreativeTab(OSMMain.osmTab);
		this.setLightLevel(15.0F);
		this.setLightOpacity(0);
		this.setStepSound(soundTypeCloth);
	}

	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new GasCloudTileEntity();
    }
	
    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int par2, int par3, int par4) {
        return null;
    }
	
	@SideOnly(Side.CLIENT)
    public int getRenderBlockPass() {
        return 1;
    }
    
	 public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
	    if (entity instanceof EntityPlayer){
	    	EntityPlayer player = (EntityPlayer) entity;
	    	player.addPotionEffect(new PotionEffect(Potion.poison.id, 50, 1));       
	    }
	 }
	 
	 @SideOnly(Side.CLIENT)
	 public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side) {
		 Block block = access.getBlock(x, y, z);

		 if (access.getBlockMetadata(x, y, z) != access.getBlockMetadata(x - Facing.offsetsXForSide[side], y - Facing.offsetsYForSide[side], z - Facing.offsetsZForSide[side])) { return true; }

		 if (block == this) { return false; }

		 return !this.something && block == this ? false : super.shouldSideBeRendered(access, x, y, z, side);
	 }
}