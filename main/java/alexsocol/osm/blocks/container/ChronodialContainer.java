package alexsocol.osm.blocks.container;

import alexsocol.osm.blocks.tileentity.ChronodialProcessorTileEntity;
import alexsocol.osm.crafting.ProcessorRecipes;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ChronodialContainer extends Container
{
    private ChronodialProcessorTileEntity tileProcessor;
    private int lastCookTime;
    private int lastBurnTime;
    private int lastItemBurnTime;
    private static final String __OBFID = "CL_00001748";

    public ChronodialContainer(InventoryPlayer ip, ChronodialProcessorTileEntity te)
    {
        this.tileProcessor = te;
        this.addSlotToContainer(new Slot(te, 0, 56, 17));
        this.addSlotToContainer(new Slot(te, 1, 56, 53));
        this.addSlotToContainer(new SlotFurnace(ip.player, te, 2, 116, 35));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(ip, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(ip, i, 8 + i * 18, 142));
        }
    }

    public void addCraftingToCrafters(ICrafting craft)
    {
        super.addCraftingToCrafters(craft);
        craft.sendProgressBarUpdate(this, 0, this.tileProcessor.completedTime);
        craft.sendProgressBarUpdate(this, 1, this.tileProcessor.processingTime);
        craft.sendProgressBarUpdate(this, 2, this.tileProcessor.currentItemProcessingTime);
    }

    /**
     * Looks for changes made in the container, sends them to every listener.
     */
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);

            if (this.lastCookTime != this.tileProcessor.completedTime)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.tileProcessor.completedTime);
            }

            if (this.lastBurnTime != this.tileProcessor.currentItemProcessingTime)
            {
                icrafting.sendProgressBarUpdate(this, 1, this.tileProcessor.currentItemProcessingTime);
            }

            if (this.lastItemBurnTime != this.tileProcessor.currentItemProcessingTime)
            {
                icrafting.sendProgressBarUpdate(this, 2, this.tileProcessor.currentItemProcessingTime);
            }
        }

        this.lastCookTime = this.tileProcessor.completedTime;
        this.lastBurnTime = this.tileProcessor.currentItemProcessingTime;
        this.lastItemBurnTime = this.tileProcessor.currentItemProcessingTime;
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2)
    {
        if (par1 == 0)
        {
            this.tileProcessor.completedTime = par2;
        }

        if (par1 == 1)
        {
            this.tileProcessor.currentItemProcessingTime = par2;
        }

        if (par1 == 2)
        {
            this.tileProcessor.currentItemProcessingTime = par2;
        }
    }

    public boolean canInteractWith(EntityPlayer p_75145_1_)
    {
        return this.tileProcessor.isUseableByPlayer(p_75145_1_);
    }

    /**
     * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
     */
    public ItemStack transferStackInSlot(EntityPlayer player, int par1)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(par1);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (par1 == 2)
            {
                if (!this.mergeItemStack(itemstack1, 3, 39, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (par1 != 1 && par1 != 0)
            {
                if (ProcessorRecipes.processing().getProcessingResult(itemstack1) != null)
                {
                    if (!this.mergeItemStack(itemstack1, 0, 1, false))
                    {
                        return null;
                    }
                }
                else if (ChronodialProcessorTileEntity.isItemFuel(itemstack1))
                {
                    if (!this.mergeItemStack(itemstack1, 1, 2, false))
                    {
                        return null;
                    }
                }
                else if (par1 >= 3 && par1 < 30)
                {
                    if (!this.mergeItemStack(itemstack1, 30, 39, false))
                    {
                        return null;
                    }
                }
                else if (par1 >= 30 && par1 < 39 && !this.mergeItemStack(itemstack1, 3, 30, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 3, 39, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(player, itemstack1);
        }

        return itemstack;
    }
}
