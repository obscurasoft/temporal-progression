package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.ForceFieldTileEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Facing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class ForceField extends BlockContainer {

	private boolean something;

	public ForceField() {
		super(Material.cake);
		this.setBlockName("ForceField");
		this.setBlockTextureName(ModInfo.MODID + ":ForceField");
		this.setBlockUnbreakable();
		this.setCreativeTab(OSMMain.osmTab);
		this.setLightLevel(15.0F);
		this.setLightOpacity(0);
		this.setStepSound(soundTypeCloth);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new ForceFieldTileEntity();
    }
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@SideOnly(Side.CLIENT)
    public int getRenderBlockPass() {
        return 1;
    }
	
	@SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side)
    {
        Block block = access.getBlock(x, y, z);
        
        if (access.getBlockMetadata(x, y, z) != access.getBlockMetadata(x - Facing.offsetsXForSide[side], y - Facing.offsetsYForSide[side], z - Facing.offsetsZForSide[side])) {
        	return true;
        }
        if (block == this) {
        	return false;
        }
        return !this.something && block == this ? false : super.shouldSideBeRendered(access, x, y, z, side);
    }
}