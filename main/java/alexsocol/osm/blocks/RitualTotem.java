//Thanks to Flenix for his tutorial 'Rendering a Techne Model as a Block' on minecraftforge.net
package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.RitualTotemTileEntity;
import alexsocol.osm.entity.HarmlessLightningBoltEntity;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class RitualTotem extends BlockContainer {
	
	public RitualTotem() {
		super(Material.rock);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 3.0F, 1.0F);
		this.setBlockName("RitualTotem");
		this.setBlockTextureName(ModInfo.MODID + ":RitualTotemIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(8.0F);
		this.setLightOpacity(1);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeStone);
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ){
		if ((world.getBlock(x - 1, y, z) == Blocks.double_plant) && (world.getBlockMetadata(x - 1, y, z) == 1) && (world.getBlock(x + 1, y, z) == Blocks.double_plant) && (world.getBlockMetadata(x + 1, y, z) == 4) && (world.getBlock(x, y, z - 1) == Blocks.red_flower) && (world.getBlockMetadata(x, y, z - 1) == 6) && (world.getBlock(x, y, z + 1) == Blocks.double_plant) && (world.getBlockMetadata(x, y, z + 1) == 0)){
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x + 3, y, z));
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x - 3, y, z));
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x - 1, y, z + 2));
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x - 1, y, z - 2));
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x + 1, y, z + 2));
			world.addWeatherEffect(new HarmlessLightningBoltEntity(world, x + 1, y, z - 2));
		}
		return super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new RitualTotemTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
