package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.RitualStoneTileEntity;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class RitualStone extends BlockContainer {
	
	public RitualStone() {
		super(Material.iron);
		this.setBlockName("RitualStone");
		this.setBlockTextureName(ModInfo.MODID + ":RitualStoneIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(8.0F);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeMetal);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new RitualStoneTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
	
	public boolean isFireBallRiteCorrect(World world, int x, int y, int z, EntityPlayer player){
		if (
		(world.getBlock(x + 1, y, z + 1) == Blocks.fire) && (world.getBlock(x + 2, y, z + 2) == Blocks.fire) && (world.getBlock(x + 3, y, z + 3) == Blocks.fire) && 
		(world.getBlock(x - 1, y, z - 1) == Blocks.fire) && (world.getBlock(x - 2, y, z - 2) == Blocks.fire) && (world.getBlock(x - 3, y, z - 3) == Blocks.fire) &&
		(world.getBlock(x + 1, y - 1, z + 1) != Blocks.netherrack) && (world.getBlock(x + 2, y - 1, z + 2) != Blocks.netherrack) && (world.getBlock(x + 3, y - 1, z + 3) != Blocks.netherrack) && 
		(world.getBlock(x - 1, y - 1, z - 1) != Blocks.netherrack) && (world.getBlock(x - 2, y - 1, z - 2) != Blocks.netherrack) && (world.getBlock(x - 3, y - 1, z - 3) != Blocks.netherrack) &&
		(world.getBlock(x + 1, y, z - 2) == Blocks.redstone_block) && (world.getBlock(x - 1, y, z + 2) == Blocks.redstone_block) &&
		(world.getBlock(x + 1, y - 1, z - 2) == Blocks.nether_brick) && (world.getBlock(x - 1, y - 1, z + 2) == Blocks.nether_brick) &&
		(world.getBlock(x + 2, y, z - 1) == Blocks.lava) && (world.getBlock(x - 2, y, z + 1) == Blocks.lava) &&
		(world.getBlock(x + 2, y - 1, z - 1) == Blocks.soul_sand) && (world.getBlock(x - 2, y - 1, z + 1) == Blocks.soul_sand) &&
		(world.getBlock(x + 3, y, z - 3) == Blocks.beacon) && (world.getBlock(x - 3, y, z + 3) == Blocks.beacon) &&
		(world.getBlock(x + 3, y - 1, z - 3) == Blocks.emerald_block) && (world.getBlock(x - 3, y - 1, z + 3) == Blocks.emerald_block) && 
		(player.experienceLevel >= 30) && (world.provider.dimensionId == -1)){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isAcidArrowRiteCorrect(World world, int x, int y, int z, EntityPlayer player){
		if (
			(world.getBlock(x + 1, y, z + 1) == Blocks.end_stone) && (world.getBlock(x + 2, y, z + 2) == Blocks.end_stone) && (world.getBlock(x + 3, y, z + 3) == Blocks.end_stone) && 
			(world.getBlock(x - 1, y, z - 1) == Blocks.end_stone) && (world.getBlock(x - 2, y, z - 2) == Blocks.end_stone) && (world.getBlock(x - 3, y, z - 3) == Blocks.end_stone) &&
			(world.getBlock(x + 1, y, z - 2) == Blocks.cactus) && (world.getBlock(x - 1, y, z + 2) == Blocks.cactus) &&
			(world.getBlock(x + 1, y - 1, z - 2) == Blocks.sand) && (world.getBlock(x - 1, y - 1, z + 2) == Blocks.sand) &&
			(world.getBlockMetadata(x + 1, y - 1, z - 2) == 1) && (world.getBlockMetadata(x - 1, y - 1, z + 2) == 1) &&
			(world.getBlock(x + 2, y, z - 1) == Blocks.red_mushroom) && (world.getBlock(x - 2, y, z + 1) == Blocks.red_mushroom) &&
			(world.getBlock(x + 2, y - 1, z - 1) == Blocks.dirt) && (world.getBlock(x - 2, y - 1, z + 1) == Blocks.dirt) &&
			(world.getBlockMetadata(x + 2, y - 1, z - 1) == 2) && (world.getBlockMetadata(x - 2, y - 1, z + 1) == 2) &&
			(world.getBlock(x + 3, y, z - 3) == Blocks.beacon) && (world.getBlock(x - 3, y, z + 3) == Blocks.beacon) &&
			(world.getBlock(x + 3, y - 1, z - 3) == Blocks.emerald_block) && (world.getBlock(x - 3, y - 1, z + 3) == Blocks.emerald_block) && 
			(player.experienceLevel >= 30) && (world.provider.dimensionId == 0)){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isFlameArrowRiteCorrect(World world, int x, int y, int z, EntityPlayer player){
		if (
			(world.getBlock(x + 1, y, z + 1) == Blocks.fire) && (world.getBlock(x + 2, y, z + 2) == Blocks.fire) && (world.getBlock(x + 3, y, z + 3) == Blocks.fire) && 
			(world.getBlock(x - 1, y, z - 1) == Blocks.fire) && (world.getBlock(x - 2, y, z - 2) == Blocks.fire) && (world.getBlock(x - 3, y, z - 3) == Blocks.fire) &&
			(world.getBlock(x + 1, y - 1, z + 1) != Blocks.netherrack) && (world.getBlock(x + 2, y - 1, z + 2) != Blocks.netherrack) && (world.getBlock(x + 3, y - 1, z + 3) != Blocks.netherrack) && 
			(world.getBlock(x - 1, y - 1, z - 1) != Blocks.netherrack) && (world.getBlock(x - 2, y - 1, z - 2) != Blocks.netherrack) && (world.getBlock(x - 3, y - 1, z - 3) != Blocks.netherrack) &&
			(world.getBlock(x + 1, y, z - 2) == Blocks.tnt) && (world.getBlock(x - 1, y, z + 2) == Blocks.tnt) &&
			(world.getBlock(x + 1, y - 1, z - 2) == Blocks.nether_brick) && (world.getBlock(x - 1, y - 1, z + 2) == Blocks.nether_brick) &&
			(world.getBlock(x + 2, y, z - 1) == Blocks.lava) && (world.getBlock(x - 2, y, z + 1) == Blocks.lava) &&
			(world.getBlock(x + 2, y - 1, z - 1) == Blocks.soul_sand) && (world.getBlock(x - 2, y - 1, z + 1) == Blocks.soul_sand) &&
			(world.getBlock(x + 3, y, z - 3) == Blocks.beacon) && (world.getBlock(x - 3, y, z + 3) == Blocks.beacon) &&
			(world.getBlock(x + 3, y - 1, z - 3) == Blocks.emerald_block) && (world.getBlock(x - 3, y - 1, z + 3) == Blocks.emerald_block) && 
			(player.experienceLevel >= 30) && (world.provider.dimensionId == -1)){
			return true;
		} else {
			return false;
		}
	}

	public boolean isLightningBoltRiteCorrect(World world, int x, int y, int z, EntityPlayer player){
		if (
			(world.getBlock(x + 1, y, z + 1) == Blocks.quartz_block) && (world.getBlock(x + 2, y, z + 2) == Blocks.quartz_block) && (world.getBlock(x + 3, y, z + 3) == Blocks.quartz_block) && 
			(world.getBlock(x - 1, y, z - 1) == Blocks.quartz_block) && (world.getBlock(x - 2, y, z - 2) == Blocks.quartz_block) && (world.getBlock(x - 3, y, z - 3) == Blocks.quartz_block) &&
			(world.getBlock(x + 1, y, z - 2) == Blocks.diamond_block) && (world.getBlock(x - 1, y, z + 2) == Blocks.diamond_block) &&
			(world.getBlock(x + 1, y - 1, z - 2) == Blocks.stonebrick) && (world.getBlock(x - 1, y - 1, z + 2) == Blocks.stonebrick) &&
			(world.getBlockMetadata(x + 1, y - 1, z - 2) == 3) && (world.getBlockMetadata(x - 1, y - 1, z + 2) == 3) &&
			(world.getBlock(x + 2, y, z - 1) == Blocks.quartz_block) && (world.getBlock(x - 2, y, z + 1) == Blocks.quartz_block) &&
			(world.getBlock(x + 2, y - 1, z - 1) == Blocks.stonebrick) && (world.getBlock(x - 2, y - 1, z + 1) == Blocks.stonebrick) &&
			(world.getBlockMetadata(x + 2, y - 1, z - 1) == 2) && (world.getBlockMetadata(x - 2, y - 1, z + 1) == 2) &&
			(world.getBlock(x + 3, y, z - 3) == Blocks.beacon) && (world.getBlock(x - 3, y, z + 3) == Blocks.beacon) &&
			(world.getBlock(x + 3, y - 1, z - 3) == Blocks.emerald_block) && (world.getBlock(x - 3, y - 1, z + 3) == Blocks.emerald_block) && 
			(player.experienceLevel >= 30) &&(world.provider.dimensionId == 1)){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if (!world.isRemote){
			if (world.getBlock(x, y - 1, z) == CommonProxy.runicStone){
				if (isFireBallRiteCorrect(world, x, y, z, player)) {
					player.addExperienceLevel(-30);
					world.setBlock(x + 1, y, z + 1, Blocks.air);
					world.setBlock(x + 2, y, z + 2, Blocks.air);
					world.setBlock(x + 3, y, z + 3, Blocks.air);
					world.setBlock(x - 1, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z - 2, Blocks.air);
					world.setBlock(x - 3, y, z - 3, Blocks.air);
					world.setBlock(x + 2, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z + 1, Blocks.air);
					world.setBlock(x + 1, y, z - 2, Blocks.air);
					world.setBlock(x - 1, y, z + 2, Blocks.air);
					/*world.addWeatherEffect(new EntityLightningBolt(world, x, y + 1, z));
					world.addWeatherEffect(new EntityLightningBolt(world, x + 3, y + 1, z - 3));
					world.addWeatherEffect(new EntityLightningBolt(world, x - 3, y + 1, z + 3));*/
					world.spawnEntityInWorld(new EntityItem(world, x, y + 1, z, new ItemStack(CommonProxy.spellFireBall)));
				}
				if (isAcidArrowRiteCorrect(world, x, y, z, player)){
					player.addExperienceLevel(-30);
					world.setBlock(x + 1, y, z + 1, Blocks.air);
					world.setBlock(x + 2, y, z + 2, Blocks.air);
					world.setBlock(x + 3, y, z + 3, Blocks.air);
					world.setBlock(x - 1, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z - 2, Blocks.air);
					world.setBlock(x - 3, y, z - 3, Blocks.air);
					world.setBlock(x + 2, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z + 1, Blocks.air);
					world.setBlock(x + 1, y, z - 2, Blocks.air);
					world.setBlock(x - 1, y, z + 2, Blocks.air);
					/*world.addWeatherEffect(new EntityLightningBolt(world, x, y + 1, z));
					world.addWeatherEffect(new EntityLightningBolt(world, x + 3, y + 1, z - 3));
					world.addWeatherEffect(new EntityLightningBolt(world, x - 3, y + 1, z + 3));*/
					world.spawnEntityInWorld(new EntityItem(world, x, y + 1, z, new ItemStack(CommonProxy.spellAcidArrow)));
				}
				if (isFlameArrowRiteCorrect(world, x, y, z, player)){
					player.addExperienceLevel(-30);
					world.setBlock(x + 1, y, z + 1, Blocks.air);
					world.setBlock(x + 2, y, z + 2, Blocks.air);
					world.setBlock(x + 3, y, z + 3, Blocks.air);
					world.setBlock(x - 1, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z - 2, Blocks.air);
					world.setBlock(x - 3, y, z - 3, Blocks.air);
					world.setBlock(x + 2, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z + 1, Blocks.air);
					world.setBlock(x + 1, y, z - 2, Blocks.air);
					world.setBlock(x - 1, y, z + 2, Blocks.air);
					/*world.addWeatherEffect(new EntityLightningBolt(world, x, y + 1, z));
					world.addWeatherEffect(new EntityLightningBolt(world, x + 3, y + 1, z - 3));
					world.addWeatherEffect(new EntityLightningBolt(world, x - 3, y + 1, z + 3));*/
					world.spawnEntityInWorld(new EntityItem(world, x, y + 1, z, new ItemStack(CommonProxy.spellFlameArrow)));
				}
				if (isLightningBoltRiteCorrect(world, x, y, z, player)){
					player.addExperienceLevel(-30);
					world.setBlock(x + 1, y, z + 1, Blocks.air);
					world.setBlock(x + 2, y, z + 2, Blocks.air);
					world.setBlock(x + 3, y, z + 3, Blocks.air);
					world.setBlock(x - 1, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z - 2, Blocks.air);
					world.setBlock(x - 3, y, z - 3, Blocks.air);
					world.setBlock(x + 2, y, z - 1, Blocks.air);
					world.setBlock(x - 2, y, z + 1, Blocks.air);
					world.setBlock(x + 1, y, z - 2, Blocks.air);
					world.setBlock(x - 1, y, z + 2, Blocks.air);
					/*world.addWeatherEffect(new EntityLightningBolt(world, x, y + 1, z));
					world.addWeatherEffect(new EntityLightningBolt(world, x + 3, y + 1, z - 3));
					world.addWeatherEffect(new EntityLightningBolt(world, x - 3, y + 1, z + 3));*/
					world.spawnEntityInWorld(new EntityItem(world, x, y + 1, z, new ItemStack(CommonProxy.spellLightningBolt)));
				}
			}
		}
		return super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);
	}
} 