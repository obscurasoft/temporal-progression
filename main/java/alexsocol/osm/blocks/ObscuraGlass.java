package alexsocol.osm.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.Facing;
import net.minecraft.world.IBlockAccess;

public class ObscuraGlass extends Block {

	private boolean something;

	public ObscuraGlass() {
		super(Material.glass);
		this.setBlockName("ObscuraGlass");
		this.setBlockTextureName(ModInfo.MODID + ":ObscuraGlass");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 1);
		this.setLightOpacity(0);
		this.setResistance(600.F);
		this.setStepSound(soundTypeStone);
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	protected boolean canSilkHarvest(){
        return true;
    }
	
	@SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side)
    {
        Block block = access.getBlock(x, y, z);
        
        if (access.getBlockMetadata(x, y, z) != access.getBlockMetadata(x - Facing.offsetsXForSide[side], y - Facing.offsetsYForSide[side], z - Facing.offsetsZForSide[side])) {
        	return true;
        }
        if (block == this) {
        	return false;
        }
        return !this.something && block == this ? false : super.shouldSideBeRendered(access, x, y, z, side);
    }
}
