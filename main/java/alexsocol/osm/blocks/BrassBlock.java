package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class BrassBlock extends Block {
	
	public BrassBlock() {
		super(Material.rock);
		this.setBlockName("BrassBlock");
		this.setBlockTextureName(ModInfo.MODID + ":BrassBlock");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.5F);
		this.setHarvestLevel("pickaxe", 2);
		this.setResistance(2000.F);
		this.setStepSound(soundTypeMetal);
	}
	
	public boolean isBeaconBase(IBlockAccess world, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
	{
		return true;
	}
}
