package alexsocol.osm.blocks.runes;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.runes.tileentity.RuneChaosTileEntity;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class RuneChaos extends BlockContainer {

	public RuneChaos() {
		super(Material.rock);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.001F, 1.0F);
		this.setBlockName("RuneChaos");
		this.setBlockTextureName(ModInfo.MODID + ":RuneChaos");
		this.setCreativeTab(OSMMain.runeTab);
		this.setHardness(0.0F);
		this.setResistance(200.F);
		this.setStepSound(soundTypeStone);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new RuneChaosTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
