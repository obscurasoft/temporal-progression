package alexsocol.osm.blocks.runes;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.runes.tileentity.RuneSonicTileEntity;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class RuneSonic extends BlockContainer {

	public RuneSonic() {
		super(Material.rock);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.001F, 1.0F);
		this.setBlockName("RuneSonic");
		this.setBlockTextureName(ModInfo.MODID + ":RuneSonic");
		this.setCreativeTab(OSMMain.runeTab);
		this.setHardness(0.0F);
		this.setResistance(200.F);
		this.setStepSound(soundTypeStone);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new RuneSonicTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
