package alexsocol.osm.blocks.runes;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.runes.tileentity.RuneAcidTileEntity;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class RuneAcid extends BlockContainer {
	
	public RuneAcid() {
		super(Material.rock);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.001F, 1.0F);
		this.setBlockName("RuneAcid");
		this.setBlockTextureName(ModInfo.MODID + ":RuneAcid");
		this.setCreativeTab(OSMMain.runeTab);
		this.setHardness(0.0F);
		this.setResistance(200.F);
		this.setStepSound(soundTypeStone);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new RuneAcidTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
