package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class ResonatingObscura extends BlockFluidClassic {
	
	@SideOnly(Side.CLIENT)
    protected IIcon stillIcon;
    @SideOnly(Side.CLIENT)
    protected IIcon flowingIcon;

    public ResonatingObscura(Fluid fluid, Material material) {
        super(fluid, material);
        this.setBlockName("ResonatingObscura");
        this.setCreativeTab(OSMMain.osmTab);
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return (side == 0 || side == 1) ? stillIcon : flowingIcon;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister register) {
        stillIcon = register.registerIcon(ModInfo.MODID + ":ResonatingObscuraStill");
        flowingIcon = register.registerIcon(ModInfo.MODID + ":ResonatingObscuraFlowing");
    }

    @Override
    public boolean canDisplace(IBlockAccess world, int x, int y, int z) {
        if (world.getBlock(x, y, z).getMaterial().isLiquid()) {
            return false;
        }
        return super.canDisplace(world, x, y, z);
    }

    @Override
    public boolean displaceIfPossible(World world, int x, int y, int z) {
        if (world.getBlock(x, y, z).getMaterial().isLiquid()) {
            return false;
        }
        return super.displaceIfPossible(world, x, y, z);
    }

    public void onEntityCollidedWithBlock(World world, int par2, int par3, int par4, Entity entity) {
        if (entity instanceof EntityLivingBase) {
            ((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.damageBoost.id, 50, 2));
        }
    }
    
    public void onBlockAdded(World world, int X, int Y, int Z) {
    	Block down = world.getBlock(X, Y - 1, Z);
    	Block east = world.getBlock(X + 1, Y, Z);
    	Block west = world.getBlock(X - 1, Y, Z);
    	Block north = world.getBlock(X, Y, Z + 1);
    	Block south = world.getBlock(X, Y, Z - 1);

    	if ((down.getBlockHardness(world, X, Y, Z) != -1) && (down != this)) {
    		world.setBlock(X, Y - 1, Z, Blocks.fire);
    	}

    	if ((east.getBlockHardness(world, X, Y, Z) != -1) && (east != this) && (world.getBlock(X, Y - 1, Z) != Blocks.air) && (world.getBlock(X + 1, Y, Z) != Blocks.air)) {
    		world.setBlock(X + 1, Y, Z, Blocks.fire);
    	}

    	if ((west.getBlockHardness(world, X, Y, Z) != -1) && (west != this) && (world.getBlock(X, Y - 1, Z) != Blocks.air) && (world.getBlock(X - 1, Y, Z) != Blocks.air)) {
    		world.setBlock(X - 1, Y, Z, Blocks.fire);
    	}

    	if ((north.getBlockHardness(world, X, Y, Z) != -1) && (north != this) && (world.getBlock(X, Y - 1, Z) != Blocks.air) && (world.getBlock(X, Y, Z + 1) != Blocks.air)) {
    		world.setBlock(X, Y, Z + 1, Blocks.fire);
    	}

    	if ((south.getBlockHardness(world, X, Y, Z) != -1) && (south != this) && (world.getBlock(X, Y - 1, Z) != Blocks.air) && (world.getBlock(X, Y, Z - 1) != Blocks.air)) {
    		world.setBlock(X, Y, Z - 1, Blocks.fire);
    	}
    }
}
