package alexsocol.osm.blocks.tileentity;

import net.minecraft.tileentity.TileEntity;

public class PurpleConTileEntity extends TileEntity {

	public float rotateF;

	public PurpleConTileEntity() {
		rotateF = 0.0F;
	}

	public void updateEntity() {
		rotateF += 1F;
		if (rotateF >= 369.9F) rotateF = 0.0F;
	}
}
