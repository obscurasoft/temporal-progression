package alexsocol.osm.blocks.tileentity;

import alexsocol.osm.crafting.ProcessorRecipes;
import alexsocol.osm.proxy.CommonProxy;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class ChronodialProcessorTileEntity extends TileEntity implements ISidedInventory {
	private static final int[] slotsTop = new int[] { 0 };
	private static final int[] slotsBottom = new int[] { 2, 1 };
	private static final int[] slotsSides = new int[] { 1 };
	/**
	 * The ItemStacks that hold the items currently being used in the furnace
	 */
	private ItemStack[] itemStacks = new ItemStack[3];
	/**
	 * The number of ticks that a fresh copy of the currently-burning item would
	 * keep the furnace burning for
	 */
	public int currentItemProcessingTime;
	/** The number of ticks that the current item has been cooking for */
	public int processingTime = 0;
	public int completedTime = 1;
	private String name;

	public int getSizeInventory() {
		return this.itemStacks.length;
	}

	public ItemStack getStackInSlot(int slot) {
		return this.itemStacks[slot];
	}

	public ItemStack decrStackSize(int slot, int amount) {
		if (this.itemStacks[slot] != null) {
			ItemStack itemstack;

			if (this.itemStacks[slot].stackSize <= amount) {
				itemstack = this.itemStacks[slot];
				this.itemStacks[slot] = null;
				return itemstack;
			} else {
				itemstack = this.itemStacks[slot].splitStack(amount);

				if (this.itemStacks[slot].stackSize == 0) {
					this.itemStacks[slot] = null;
				}

				return itemstack;
			}
		} else {
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop
	 * whatever it returns as an EntityItem - like when you close a workbench
	 * GUI.
	 */
	public ItemStack getStackInSlotOnClosing(int slot) {
		if (this.itemStacks[slot] != null) {
			ItemStack itemstack = this.itemStacks[slot];
			this.itemStacks[slot] = null;
			return itemstack;
		} else {
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be
	 * crafting or armor sections).
	 */
	public void setInventorySlotContents(int slot, ItemStack stack) {
		this.itemStacks[slot] = stack;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
			stack.stackSize = this.getInventoryStackLimit();
		}
	}

	/**
	 * Returns the name of the inventory
	 */
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.name : "container.processor";
	}

	/**
	 * Returns if the inventory is named
	 */
	public boolean hasCustomInventoryName() {
		return this.name != null && this.name.length() > 0;
	}

	public void getName(String str) {
		this.name = str;
	}

	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		NBTTagList nbttaglist = tag.getTagList("Items", 10);
		this.itemStacks = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i) {
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");

			if (b0 >= 0 && b0 < this.itemStacks.length) {
				this.itemStacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		this.processingTime = tag.getShort("ProcessingTime");
		this.currentItemProcessingTime = getItemWorkTime(this.itemStacks[1]);

		if (tag.hasKey("CustomName", 8)) {
			this.name = tag.getString("CustomName");
		}
	}

	public void writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
		tag.setShort("ProcessingTime", (short) this.processingTime);
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.itemStacks.length; ++i) {
			if (this.itemStacks[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.itemStacks[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		tag.setTag("Items", nbttaglist);

		if (this.hasCustomInventoryName()) {
			tag.setString("CustomName", this.name);
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot.
	 */
	public int getInventoryStackLimit() {
		return 64;
	}

	/**
	 * Returns an integer between 0 and the passed value representing how close
	 * the current item is to being completely cooked
	 */
	@SideOnly(Side.CLIENT)
	public int getCookProgressScaled(int time) {
		return this.processingTime * time / 200;
	}

	/**
	 * Returns an integer between 0 and the passed value representing how much
	 * burn time is left on the current fuel item, where 0 means that the item
	 * is exhausted and the passed value means that the item is fresh
	 */
	@SideOnly(Side.CLIENT)
	public int getBurnTimeRemainingScaled(int time) {
		if (this.currentItemProcessingTime == 0) {
			this.currentItemProcessingTime = 200;
		}

		return this.processingTime * time / this.currentItemProcessingTime;
	}

	public boolean isProcessing() {
		return this.processingTime > 0;
	}

	public void updateEntity() {
		boolean flag = this.processingTime > 0;
		boolean flag1 = false;

		if (this.processingTime > 0) {
			--this.processingTime;
		}

		if (!this.worldObj.isRemote) {
			if (this.processingTime != 0 || this.itemStacks[1] != null && this.itemStacks[0] != null) {
				if (this.processingTime == 0 && this.canProcess()) {
					this.currentItemProcessingTime = this.processingTime = getItemWorkTime(this.itemStacks[1]);
					if (this.processingTime > 0) {
						flag1 = true;

						if (this.itemStacks[1] != null) {
							--this.itemStacks[1].stackSize;

							if (this.itemStacks[1].stackSize == 0) {
								this.itemStacks[1] = itemStacks[1].getItem().getContainerItem(itemStacks[1]);
							}
						}
					}
				}

				if (this.isProcessing() && this.canProcess()) {
					++this.completedTime;

					if (this.completedTime == currentItemProcessingTime) {
						this.completedTime = 0;
						this.processItem();
						flag1 = true;
					}
				} else {
					this.completedTime = 0;
				}
			}

			if (flag != this.processingTime > 0) {
				flag1 = true;
				BlockFurnace.updateFurnaceBlockState(this.processingTime > 0, this.worldObj, this.xCoord, this.yCoord,
						this.zCoord);
			}
		}

		if (flag1) {
			this.markDirty();
		}
	}

	private boolean canProcess() {
		if (this.itemStacks[0] == null) {
			return false;
		} else {
			ItemStack itemstack = ProcessorRecipes.processing().getProcessingResult(this.itemStacks[0]);
			if (itemstack == null)
				return false;
			if (this.itemStacks[2] == null)
				return true;
			if (!this.itemStacks[2].isItemEqual(itemstack))
				return false;
			int result = itemStacks[2].stackSize + itemstack.stackSize;
			return result <= getInventoryStackLimit() && result <= this.itemStacks[2].getMaxStackSize();
		}
	}

	public void processItem() {
		if (this.canProcess()) {
			ItemStack itemstack = ProcessorRecipes.processing().getProcessingResult(this.itemStacks[0]);

			if (this.itemStacks[2] == null) {
				this.itemStacks[2] = itemstack.copy();
			} else if (this.itemStacks[2].getItem() == itemstack.getItem()) {
				this.itemStacks[2].stackSize += itemstack.stackSize;
			}

			--this.itemStacks[0].stackSize;

			if (this.itemStacks[0].stackSize <= 0) {
				this.itemStacks[0] = null;
			}
		}
	}

	public static int getItemWorkTime(ItemStack stack) {
		if (stack.getItem() == CommonProxy.strangeMatter) {
			return 800;
		}
		return 0;
	}

	public static boolean isItemFuel(ItemStack stack) {
		return getItemWorkTime(stack) > 0;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes
	 * with Container
	 */
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false
				: player.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D,
						(double) this.zCoord + 0.5D) <= 64.0D;
	}

	public void openInventory() {
	}

	public void closeInventory() {
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring
	 * stack size) into the given slot.
	 */
	public boolean isItemValidForSlot(int slot, ItemStack stack) {
		return slot == 2 ? false : (slot == 1 ? isItemFuel(stack) : true);
	}

	/**
	 * Returns an array containing the indices of the slots that can be accessed
	 * by automation on the given side of this block.
	 */
	public int[] getAccessibleSlotsFromSide(int slot) {
		return slot == 0 ? slotsBottom : (slot == 1 ? slotsTop : slotsSides);
	}

	/**
	 * Returns true if automation can insert the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	public boolean canInsertItem(int par1, ItemStack stack, int par3) {
		return this.isItemValidForSlot(par1, stack);
	}

	/**
	 * Returns true if automation can extract the given item in the given slot
	 * from the given side. Args: Slot, item, side
	 */
	public boolean canExtractItem(int par1, ItemStack stack, int par3) {
		return par3 != 0 || par1 != 1 || stack.getItem() == Items.bucket;
	}
}