package alexsocol.osm.blocks.tileentity;

import net.minecraft.tileentity.TileEntity;

public class AbstractEffectTileEntity extends TileEntity {
	
	public float rotateF;

	public AbstractEffectTileEntity() {
		rotateF = 0.0F;
	}

	public void updateEntity() {
		rotateF += 1F;
		if (rotateF >= 369.9F) rotateF = 0.0F;
	}
}
