package alexsocol.osm.blocks.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class ForceFieldTileEntity extends TileEntity {
	
	private int timer;
	
	@Override
    public void updateEntity() {
        timer++;
        if (timer >= 200) {
            if (!worldObj.isRemote) {
                worldObj.setBlockToAir(xCoord, yCoord, zCoord);
            }
        }
    }
	
	@Override
    public void readFromNBT(NBTTagCompound nbt) {
        timer = nbt.getInteger("time");
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
    	nbt.setInteger("time", timer);
    }
}
