package alexsocol.osm.blocks;

import java.util.Random;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.ChronodialProcessorTileEntity;
import alexsocol.osm.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ChronodialProcessor extends BlockContainer {
	private static boolean hasTileEntity;
	private final Random rnd = new Random();
	private final boolean work;
	private static boolean isActive;

	public ChronodialProcessor() {
		super(Material.iron);
		this.setBlockName("ChronodialProcessor");
		this.setBlockTextureName(ModInfo.MODID + ":ChronodialProcessorIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(15.0F);
		this.setLightOpacity(1);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeMetal);
		this.blockParticleGravity = 1.0F;
		this.slipperiness = 0.6F;
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		this.lightOpacity = 20;
		this.setTickRandomly(false);
		this.useNeighborBrightness = false;
		this.isActive = false;
		this.work = isActive;
	}

	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4, Block par5, int par6) {
		dropItemsOnBlockBreak(par1World, par2, par3, par4, par5, par6);
		ChronodialProcessorTileEntity table = (ChronodialProcessorTileEntity) par1World.getTileEntity(par2, par3, par4);
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	public static void dropItemsOnBlockBreak(World par1World, int par2, int par3, int par4, Block par5, int par6) {
		try {
			IInventory inv = (IInventory) par1World.getTileEntity(par2, par3, par4);

			if (inv != null) {
				for (int j1 = 0; j1 < inv.getSizeInventory(); ++j1) {
					ItemStack itemstack = inv.getStackInSlot(j1);

					if (itemstack != null) {
						float f = par1World.rand.nextFloat() * 0.8F + 0.1F;
						float f1 = par1World.rand.nextFloat() * 0.8F + 0.1F;
						float f2 = par1World.rand.nextFloat() * 0.8F + 0.1F;

						while (itemstack.stackSize > 0) {
							int k1 = par1World.rand.nextInt(21) + 10;

							if (k1 > itemstack.stackSize) {
								k1 = itemstack.stackSize;
							}

							itemstack.stackSize -= k1;
							EntityItem entityitem = new EntityItem(par1World, (double) ((float) par2 + f),
									(double) ((float) par3 + f1), (double) ((float) par4 + f2),
									new ItemStack(itemstack.getItem(), k1, itemstack.getItemDamage()));

							if (itemstack.hasTagCompound()) {
								entityitem.getEntityItem()
										.setTagCompound((NBTTagCompound) itemstack.getTagCompound().copy());
							}

							float f3 = 0.05F;
							entityitem.motionX = (double) ((float) par1World.rand.nextGaussian() * f3);
							entityitem.motionY = (double) ((float) par1World.rand.nextGaussian() * f3 + 0.2F);
							entityitem.motionZ = (double) ((float) par1World.rand.nextGaussian() * f3);
							par1World.spawnEntityInWorld(entityitem);
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
	}
	
    public static void updateFurnaceBlockState(boolean active, World world, int x, int y, int z)
    {
        int l = world.getBlockMetadata(x, y, z);
        TileEntity tileentity = world.getTileEntity(x, y, z);
        isActive = true;
        world.setBlock(x, y, z, CommonProxy.chronodialProcessor);

        isActive = false;
        world.setBlockMetadataWithNotify(x, y, z, l, 2);

        if (tileentity != null)
        {
            tileentity.validate();
            world.setTileEntity(x, y, z, tileentity);
        }
    }

	@Override
	public int getRenderBlockPass() {
		return 0;
	}

	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer,
			int par6, float par7, float par8, float par9) {
		if (par1World.isRemote) {
			return true;
		} else {
			if (!par5EntityPlayer.isSneaking()) {
				ItemStack currentItem = par5EntityPlayer.getCurrentEquippedItem();
				par5EntityPlayer.openGui(OSMMain.instance, 0, par1World, par2, par3, par4);
				return true;
			} else {
				ChronodialProcessorTileEntity table = (ChronodialProcessorTileEntity) par1World.getTileEntity(par2,
						par3, par4);
				return false;
			}
		}
	}

	@Override
	public int getRenderType() {
		return 2634;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new ChronodialProcessorTileEntity();
	}
}
