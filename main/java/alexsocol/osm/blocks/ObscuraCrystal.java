package alexsocol.osm.blocks;

import java.util.Random;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.ObscuraCrystalTileEntity;
import alexsocol.osm.proxy.CommonProxy;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ObscuraCrystal extends BlockContainer {
	
	public ObscuraCrystal() {
		super(Material.glass);
		this.setBlockName("ObscuraCrystal");
		this.setBlockTextureName(ModInfo.MODID + ":ObscuraCrystalIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(8.0F);
		this.setLightOpacity(0);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeGlass);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new ObscuraCrystalTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World world, int X, int Y, int Z, Random random) {
        for (int l = 0; l < 4; ++l) {
            if (world.getBlock(X, Y - 1, Z) == CommonProxy.obscuriteBlock) {
            	world.spawnParticle("portal", X, Y, Z, 0.0D, 0.0D, 0.0D);
            }
        }
    }
}
