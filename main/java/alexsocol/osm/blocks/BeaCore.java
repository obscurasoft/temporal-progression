package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BeaCore extends Block {
	
	public IIcon[] icons = new IIcon[6];

	public BeaCore() {
		super(Material.dragonEgg);
		this.setBlockName("BeaCore");
		this.setBlockTextureName(ModInfo.MODID + ":BeaCore");
		this.setCreativeTab(OSMMain.osmTab);
		this.setLightLevel(15.0F);
		this.setLightOpacity(0);
		this.setStepSound(soundTypeGlass);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister ireg){
		this.icons[0] = ireg.registerIcon(ModInfo.MODID + ":BeaCore_Bottom");
		this.icons[1] = ireg.registerIcon(ModInfo.MODID + ":BeaCore_Top");
		for (int i = 2; i < 6; i++) {
			this.icons[i] = ireg.registerIcon(ModInfo.MODID + ":BeaCore_Side");
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int meta) {
		return this.icons[side];
	}
	
	/*
	//������ �������� ���������� �������
	public boolean isSphereBuild(World world, int X, int Y, int Z){
		int correctNumber = 0;
		
		for (int x = -1; x < 2; x++){
			for (int z = -1; z < 2; z++){
				if (world.getBlock(X + x, Y - 3, Z + z) == CommonProxy.magicalCasing){
					correctNumber++;
				}
			}
		}
		
		//������� ������� ���������� �������
		for (int x = -1; x < 2; x++){
			for (int z = -1; z < 2; z++){
				if (world.getBlock(X + x, Y + 3, Z + z) == CommonProxy.magicalCasing){
					correctNumber++;
				}
			}
		}
		
		if(correctNumber == 18) {
			return true;
		} else {
			return false;
		}
	}
	
	public void onUpdate(World world, int X, int Y, int Z){
		if (isSphereBuild(world, Z, Z, Z)){
			world.setBlock(X, Y, Z, CommonProxy.purpleCon);
		}
	}
	*/
}
