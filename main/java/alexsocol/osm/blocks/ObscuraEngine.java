package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.tileentity.ObscuraCrystalTileEntity;
import alexsocol.osm.blocks.tileentity.ObscuraEngineTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class ObscuraEngine extends BlockContainer {
	public ObscuraEngine() {
		super(Material.iron);
		this.setBlockName("ObscuraEngine");
		this.setBlockTextureName(ModInfo.MODID + ":ObscuraEngineIcon");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setLightLevel(5.0F);
		this.setLightOpacity(0);
		this.setResistance(6000.F);
		this.setStepSound(soundTypeGlass);
	}
	
	@Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new ObscuraEngineTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
