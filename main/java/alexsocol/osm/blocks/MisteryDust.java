package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class MisteryDust extends Block {

	public MisteryDust() {
		super(Material.sand);
		this.setBlockName("MisteryDust");
		this.setBlockTextureName(ModInfo.MODID + ":MisteryDust");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(0.5F);
		this.setLightLevel(5.0F);
		this.setResistance(600.F);
		this.setStepSound(soundTypeSand);
	}
}
