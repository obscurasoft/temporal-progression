package alexsocol.osm.blocks;

import alexsocol.osm.ModInfo;
import alexsocol.osm.OSMMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class ObscuriteBlock extends Block {

	public ObscuriteBlock() {
		super(Material.rock);
		this.setBlockName("ObscuriteBlock");
		this.setBlockTextureName(ModInfo.MODID + ":ObscuriteBlock");
		this.setCreativeTab(OSMMain.osmTab);
		this.setHardness(1.5F);
		this.setHarvestLevel("pickaxe", 2);
		this.setResistance(2000.F);
		this.setStepSound(soundTypeStone);
	}
	
	public boolean isBeaconBase(IBlockAccess world, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
	{
		return true;
	}
}
