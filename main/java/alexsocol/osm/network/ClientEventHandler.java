package alexsocol.osm.network;

import alexsocol.osm.OSMMain;
import alexsocol.osm.proxy.CommonProxy;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Facing;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class ClientEventHandler{
	@SubscribeEvent
	public void onBlockHighlight(DrawBlockHighlightEvent event){
	}

	@SubscribeEvent
	public void onItemTooltip(ItemTooltipEvent event){
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onGuiRender(RenderGameOverlayEvent event){
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onPlayerRender(RenderPlayerEvent.Pre event){
		
	}

	@SubscribeEvent
	public void onSetArmorModel(RenderPlayerEvent.SetArmorModel event){
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onPlayerRender(RenderPlayerEvent.Post event){
	}

	@SubscribeEvent
	public void onEntityJoinWorld(EntityJoinWorldEvent event){
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent event){
	}
}
