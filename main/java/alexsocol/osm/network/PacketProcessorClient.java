package alexsocol.osm.network;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientCustomPacketEvent;
import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.server.FMLServerHandler;
import io.netty.buffer.ByteBufInputStream;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import java.io.File;
import java.io.IOException;

import alexsocol.osm.LogHelper;
import alexsocol.osm.OSMMain;

public class PacketProcessorClient extends PacketProcessor{

	@SubscribeEvent
	public void onPacketData(ClientCustomPacketEvent event){
		ByteBufInputStream bbis = new ByteBufInputStream(event.packet.payload());
		byte packetID = -1;
		try{
			if (event.packet.getTarget() != Side.CLIENT){
				return;
			}

			//constant details all packets share:  ID, player, and remaining data
			packetID = bbis.readByte();
			EntityPlayer player = Minecraft.getMinecraft().thePlayer;
			byte[] remaining = new byte[bbis.available()];
			bbis.readFully(remaining);

			switch (packetID){
			case Packets.PARTICLE_SPAWN_SPECIAL:
				break;
			case Packets.NBT_DUMP:
				break;
			case Packets.SYNC_WORLD_NAME:
				break;
			}
		}catch (Throwable t){
			LogHelper.error("Client Packet Failed to Handle!");
			LogHelper.error("Packet Type: " + packetID);
			t.printStackTrace();
		}finally{
			try{
				if (bbis != null)
					bbis.close();
			}catch (Throwable t){
				t.printStackTrace();
			}
		}
	}
}
