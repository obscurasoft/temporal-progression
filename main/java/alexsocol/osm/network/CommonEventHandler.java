package alexsocol.osm.network;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;
import net.minecraft.block.Block;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.event.brewing.PotionBrewedEvent;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.*;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.AchievementEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import alexsocol.osm.proxy.CommonProxy;

public class CommonEventHandler{

	@SubscribeEvent
	public void onPotionBrewed(PotionBrewedEvent brewEvent){
	}

	@SubscribeEvent
	public void onEndermanTeleport(EnderTeleportEvent event){
		
	}

	@SubscribeEvent
	public void onEntityConstructing(EntityConstructing event){
		
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onEntityDeath(LivingDeathEvent event){
		
	}

	@SubscribeEvent
	public void onPlayerGetAchievement(AchievementEvent event){
	}

	@SubscribeEvent
	public void onLivingDrops(LivingDropsEvent event){
	}

	@SubscribeEvent
	public void onEntityJump(LivingJumpEvent event){
	}

	@SubscribeEvent
	public void onEntityFall(LivingFallEvent event){

		EntityLivingBase ent = event.entityLiving;
		float f = event.distance;
		ent.isAirBorne = false;

		if (event.entityLiving instanceof EntityPlayer && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3] != null && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3].getItem() == CommonProxy.stasis){
			event.setCanceled(true);
			ent.fallDistance = 0;
			return;
		}
	}

	@SubscribeEvent
	public void onEntityLiving(LivingUpdateEvent event){
	}

	@SubscribeEvent
	public void onBucketFill(FillBucketEvent event){
		ItemStack result = attemptFill(event.world, event.target);

		if (result != null){
			event.result = result;
			event.setResult(Result.ALLOW);
		}
	}

	private ItemStack attemptFill(World world, MovingObjectPosition p){
		Block block = world.getBlock(p.blockX, p.blockY, p.blockZ);

		if (block == CommonProxy.obscuraFluid){
			if (world.getBlockMetadata(p.blockX, p.blockY, p.blockZ) == 0) // Check that it is a source block
			{
				world.setBlock(p.blockX, p.blockY, p.blockZ, Blocks.air); // Remove the fluid block
				return new ItemStack(CommonProxy.resonatingObscuraBucket);
			}
		}

		return null;
	}

	@SubscribeEvent
	public void onEntityInteract(EntityInteractEvent event){
	}

	@SubscribeEvent
	public void onPlayerTossItem(ItemTossEvent event){
	}

	@SubscribeEvent
	public void onEntityAttacked(LivingAttackEvent event){
		if (event.source.isFireDamage() && event.entityLiving instanceof EntityPlayer && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3] != null && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3].getItem() == CommonProxy.stasis){
			event.setCanceled(true);
			return;
		}
	}

	@SubscribeEvent
	public void onEntityHurt(LivingHurtEvent event){

		if (event.source.isFireDamage() && event.entityLiving instanceof EntityPlayer && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3] != null && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3].getItem() == CommonProxy.stasis){
			event.ammount /= 4;
			event.setCanceled(true);
			return;
		}
	}

	@SubscribeEvent
	public void onEntityJoinWorld(EntityJoinWorldEvent event){
		
	}

	@SubscribeEvent
	public void onBreakSpeed(BreakSpeed event){
		if (event.entityLiving instanceof EntityPlayer && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3] != null && ((EntityPlayer)event.entityLiving).inventory.armorInventory[3].getItem() == CommonProxy.stasis){
			event.newSpeed = event.originalSpeed * 2;
			return;
		}
	}

	@SubscribeEvent
	public void onPlayerPickupItem(EntityItemPickupEvent event){
		if (event.entityPlayer == null)
			return;

		if (event.item.getEntityItem().getItem() == CommonProxy.spellLightningBolt){
		}
	}
}


