package alexsocol.osm.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import alexsocol.osm.LogHelper;

public class DataListener {
	ByteArrayInputStream input;
	DataInputStream dataStream;
	public byte ID;

	public DataListener(byte[] data) {
		this(data, true);
	}

	public DataListener(byte[] data, boolean getID) {
		input = new ByteArrayInputStream(data);
		dataStream = new DataInputStream(input);

		// get id byte
		if (getID) {
			try {
				ID = dataStream.readByte();
			} catch (IOException e) {
				LogHelper.error("DataListener (getID): " + e.toString());
				e.printStackTrace();
			}
		}
	}

	public int getInt() {
		int value = 0;
		try {
			value = dataStream.readInt();
		} catch (IOException e) {
			LogHelper.error("DataListener (getInt): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public float getFloat() {
		float value = 0;
		try {
			value = dataStream.readFloat();
		} catch (IOException e) {
			LogHelper.error("DataListener (getFloat): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public double getDouble() {
		double value = 0;
		try {
			value = dataStream.readDouble();
		} catch (IOException e) {
			LogHelper.error("DataListener (getDouble): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public boolean getBoolean() {
		boolean value = false;
		try {
			value = dataStream.readBoolean();
		} catch (IOException e) {
			LogHelper.error("DataListener (getBoolean): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public String getString() {
		String value = "";
		try {
			value = dataStream.readUTF();
		} catch (IOException e) {
			LogHelper.error("DataListener (getString): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public byte getByte() {
		byte value = 0;
		try {
			value = dataStream.readByte();
		} catch (IOException e) {
			LogHelper.error("DataListener (getByte): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public short getShort() {
		short value = 0;
		try {
			value = dataStream.readShort();
		} catch (IOException e) {
			LogHelper.error("DataListener (getShort): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public long getLong() {
		long value = 0;
		try {
			value = dataStream.readLong();
		} catch (IOException e) {
			LogHelper.error("DataListener (getLong): " + e.toString());
			e.printStackTrace();
		}
		return value;
	}

	public NBTTagCompound getNBTTagCompound() {
		NBTTagCompound data = null;
		try {
			int len = dataStream.readInt();
			byte[] bytes = new byte[len];
			dataStream.read(bytes);
			ByteBuf buf = Unpooled.copiedBuffer(bytes);
			data = ByteBufUtils.readTag(buf);
		} catch (IOException e) {
			LogHelper.error("DataListener (getNBTTagCompound): " + e.toString());
			e.printStackTrace();
		}
		return data;
	}

	public byte[] getRemainingBytes() {
		byte[] remaining = null;
		try {
			remaining = new byte[dataStream.available()];
			dataStream.read(remaining);
		} catch (IOException e) {
			LogHelper.error("DataListener (getRemainingBytes): " + e.toString());
			e.printStackTrace();
		}

		return remaining;
	}

	public ItemStack getItemStack() {
		NBTTagCompound compound = getNBTTagCompound();
		if (compound == null)
			return null;
		ItemStack stack = ItemStack.loadItemStackFromNBT(compound);
		return stack;
	}

	public int[] getIntArray() {
		try {
			int[] arr = new int[dataStream.readInt()];
			for (int i = 0; i < arr.length; ++i)
				arr[i] = dataStream.readInt();
			return arr;
		} catch (IOException e) {
			LogHelper.error("DataListener (getIntArray): " + e.toString());
			e.printStackTrace();
		}

		return new int[0];
	}
}
