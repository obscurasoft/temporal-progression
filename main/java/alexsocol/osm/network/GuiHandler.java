package alexsocol.osm.network;

import alexsocol.osm.blocks.container.ChronodialContainer;
import alexsocol.osm.blocks.tileentity.ChronodialProcessorTileEntity;
import alexsocol.osm.gui.GuiChronodial;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {
	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = world.getTileEntity(x, y, z);
		if(te instanceof ChronodialProcessorTileEntity) {
			return new ChronodialContainer(player.inventory, (ChronodialProcessorTileEntity) te);
		}
		return null;
	}
	
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = world.getTileEntity(x,  y,  z);
		if(te instanceof ChronodialProcessorTileEntity) {
			return new GuiChronodial(player.inventory, (ChronodialProcessorTileEntity) te);
		}
		return null;
	}
}
