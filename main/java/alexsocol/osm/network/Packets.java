package alexsocol.osm.network;


public class Packets {
	public static final byte PARTICLE_SPAWN_SPECIAL = 1;
	public static final byte SYNC_WORLD_NAME = 2;
	public static final byte NBT_DUMP = 3;
	public static final byte EXPERIMENTAL_PACKET = 4;
}
