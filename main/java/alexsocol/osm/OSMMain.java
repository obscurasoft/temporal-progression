package alexsocol.osm;

import java.io.File;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import alexsocol.osm.entity.AcidArrowEntity;
import alexsocol.osm.entity.FireBallEntity;
import alexsocol.osm.entity.FlameArrowEntity;
import alexsocol.osm.entity.GasSphereEntity;
import alexsocol.osm.entity.HarmlessLightningBoltEntity;
import alexsocol.osm.entity.IceBallEntity;
import alexsocol.osm.entity.IceBoulderEntity;
import alexsocol.osm.entity.SlowlyMovingUnstopableAbstractEntity;
import alexsocol.osm.network.GuiHandler;
import alexsocol.osm.network.NetPacketHandler;
import alexsocol.osm.network.PacketProcessor;
import alexsocol.osm.proxy.CommonProxy;
import alexsocol.osm.proxy.TickHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;

@Mod(modid = ModInfo.MODID, name = ModInfo.NAME, version = ModInfo.VERSION)

public class OSMMain {
	public static OSMConfig config;

	@Instance(ModInfo.MODID)
	public static OSMMain instance;

	@SidedProxy(clientSide = "alexsocol.osm.proxy.ClientProxy", serverSide = "alexsocol.osm.proxy.CommonProxy")
	public static CommonProxy proxy;

	private TickHandler serverTickHandler;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		LogHelper.info("Loading OldSchool Magic started!");
		config = new OSMConfig(new File("AM2.cfg"));
		LogHelper.info("Loading OSM packet handlers");
		NetPacketHandler.INSTANCE.init();
		serverTickHandler = new TickHandler();
		FMLCommonHandler.instance().bus().register(serverTickHandler);
		NetPacketHandler.INSTANCE.registerChannels(new PacketProcessor());
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
		proxy.preinit();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		// ___________________________________________ENTITY
		LogHelper.info("Registering OSM Entities");
		EntityRegistry.registerModEntity(SlowlyMovingUnstopableAbstractEntity.class, "SMUAE", 1500, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(SlowlyMovingUnstopableAbstractEntity.class, "SMUAE", EntityRegistry.findGlobalUniqueEntityId());
		
		EntityRegistry.registerModEntity(AcidArrowEntity.class, "AcidArrow", 1501, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(AcidArrowEntity.class, "AcidArrow", EntityRegistry.findGlobalUniqueEntityId());

		EntityRegistry.registerModEntity(FireBallEntity.class, "FireBall", 1502, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(FireBallEntity.class, "FireBall", EntityRegistry.findGlobalUniqueEntityId());
		
		EntityRegistry.registerModEntity(FlameArrowEntity.class, "FlameArrow", 1503, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(FlameArrowEntity.class, "FlameArrow", EntityRegistry.findGlobalUniqueEntityId());

		EntityRegistry.registerModEntity(GasSphereEntity.class, "GasSphere", 1504, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(GasSphereEntity.class, "GasSphere", EntityRegistry.findGlobalUniqueEntityId());
		
		EntityRegistry.registerModEntity(HarmlessLightningBoltEntity.class, "HarmlessLightningBolt", 1505, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(HarmlessLightningBoltEntity.class, "HarmlessLightningBolt", EntityRegistry.findGlobalUniqueEntityId());

		EntityRegistry.registerModEntity(IceBallEntity.class, "IceBall", 1506, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(IceBallEntity.class, "IceBall", EntityRegistry.findGlobalUniqueEntityId());

		EntityRegistry.registerModEntity(IceBoulderEntity.class, "IceBoulder", 1507, this, 250, 20, true);
		EntityRegistry.registerGlobalEntityID(IceBoulderEntity.class, "IceBoulder", EntityRegistry.findGlobalUniqueEntityId());
		proxy.init();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postinit();
		// ___________________________________________RENDER
		LogHelper.info("Registering OSM renderers");
		proxy.RegisterRenderThings();
	}

	public static CreativeTabs osmTab = new CreativeTabs("OldschoolMagicTab") {
		@Override
		public Item getTabIconItem() {
			return new ItemStack(CommonProxy.resonator).getItem();
		}
	};
	
	public static CreativeTabs runeTab = new CreativeTabs("OldschoolMagicRunesTab") {
		@Override
		public Item getTabIconItem() {
			return Item.getItemFromBlock(CommonProxy.runicStone);
		}
	};
	
	public static CreativeTabs spellTab = new CreativeTabs("OldschoolMagicSpellsTab") {
		@Override
		public Item getTabIconItem() {
			return new ItemStack(CommonProxy.spellFireBall).getItem();
		}
	};
}