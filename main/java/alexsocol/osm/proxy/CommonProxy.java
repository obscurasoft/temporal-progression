package alexsocol.osm.proxy;

import alexsocol.osm.LogHelper;
import alexsocol.osm.OSMMain;
import alexsocol.osm.blocks.AbstractEffect;
import alexsocol.osm.blocks.BeaCore;
import alexsocol.osm.blocks.BlackFlame;
import alexsocol.osm.blocks.BrassBlock;
import alexsocol.osm.blocks.ChronodialProcessor;
import alexsocol.osm.blocks.CorupteCon;
import alexsocol.osm.blocks.ForceField;
import alexsocol.osm.blocks.GasCloud;
import alexsocol.osm.blocks.MagicalCasing;
import alexsocol.osm.blocks.MisteryDust;
import alexsocol.osm.blocks.ObscuraCrystal;
import alexsocol.osm.blocks.ObscuraGlass;
import alexsocol.osm.blocks.ObscuriteBlock;
import alexsocol.osm.blocks.ObscuriteOre;
import alexsocol.osm.blocks.PurpleCon;
import alexsocol.osm.blocks.ResonatingObscura;
import alexsocol.osm.blocks.RitualStone;
import alexsocol.osm.blocks.RitualTotem;
import alexsocol.osm.blocks.RunicStone;
import alexsocol.osm.blocks.runes.RuneAcid;
import alexsocol.osm.blocks.runes.RuneChaos;
import alexsocol.osm.blocks.runes.RuneDeath;
import alexsocol.osm.blocks.runes.RuneDeseases;
import alexsocol.osm.blocks.runes.RuneDivine;
import alexsocol.osm.blocks.runes.RuneElectro;
import alexsocol.osm.blocks.runes.RuneEvil;
import alexsocol.osm.blocks.runes.RuneFear;
import alexsocol.osm.blocks.runes.RuneFire;
import alexsocol.osm.blocks.runes.RuneFrost;
import alexsocol.osm.blocks.runes.RuneGood;
import alexsocol.osm.blocks.runes.RuneLaw;
import alexsocol.osm.blocks.runes.RuneMind;
import alexsocol.osm.blocks.runes.RuneNegative;
import alexsocol.osm.blocks.runes.RunePositive;
import alexsocol.osm.blocks.runes.RuneSonic;
import alexsocol.osm.blocks.tileentity.AbstractEffectTileEntity;
import alexsocol.osm.blocks.tileentity.ChronodialProcessorTileEntity;
import alexsocol.osm.blocks.tileentity.CorupteConTileEntity;
import alexsocol.osm.blocks.tileentity.ObscuraCrystalTileEntity;
import alexsocol.osm.blocks.tileentity.ObscuraEngineTileEntity;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;
import alexsocol.osm.blocks.tileentity.RitualTotemTileEntity;
import alexsocol.osm.items.ArtronSword;
import alexsocol.osm.items.BrassIngot;
import alexsocol.osm.items.BrassNugget;
import alexsocol.osm.items.Catcher;
import alexsocol.osm.items.Crystal;
import alexsocol.osm.items.DarkDust;
import alexsocol.osm.items.EmeraldStaff;
import alexsocol.osm.items.InactiveResonator;
import alexsocol.osm.items.MisteryCrystal;
import alexsocol.osm.items.MisteryCrystalCondenser;
import alexsocol.osm.items.MisteryCrystalCondenserActive;
import alexsocol.osm.items.ObscuriteChunk;
import alexsocol.osm.items.ResonatingObscuraBucket;
import alexsocol.osm.items.Resonator;
import alexsocol.osm.items.Sabre;
import alexsocol.osm.items.Stasis;
import alexsocol.osm.items.StrangeMatter;
import alexsocol.osm.items.UltraCheese;
import alexsocol.osm.items.XPStorage;
import alexsocol.osm.items.spells.AcidArrowSpell;
import alexsocol.osm.items.spells.ExplosionSpell;
import alexsocol.osm.items.spells.FireBallSpell;
import alexsocol.osm.items.spells.FlameArrowSpell;
import alexsocol.osm.items.spells.ForceFieldSpell;
import alexsocol.osm.items.spells.GasCloudSpell;
import alexsocol.osm.items.spells.HealingAugmentedSpell;
import alexsocol.osm.items.spells.HealingCircleSpell;
import alexsocol.osm.items.spells.HealingMundaneSpell;
import alexsocol.osm.items.spells.HealingNormalSpell;
import alexsocol.osm.items.spells.IceStormSpell;
import alexsocol.osm.items.spells.LightningBoltSpell;
import alexsocol.osm.items.spells.MeteoricStrikeSpell;
import alexsocol.osm.items.spells.WitheringTentaclesSpell;
import alexsocol.osm.network.CommonEventHandler;
import alexsocol.osm.network.NetPacketHandler;
import alexsocol.osm.network.PacketProcessor;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class CommonProxy {

	// Blocks
	public static Block abstractEffect;
	public static Block beacore;
//public static Block blackFlame;
	public static Block brassBlock;
	public static Block chronodialProcessor;
	public static Block corupteCon;
	public static Block forceField;
	public static Block gasCloud;
	public static Block magicalCasing;
	public static Block obscuraCrystal;
	public static Block obscuraFluid;
	public static Block obscuraGlass;
	public static Block obscuraSand;
	public static Block obscuriteOre;
	public static Block obscuriteBlock;
	public static Block purpleCon;
	public static Block riteStone;
	public static Block runicStone;	
	public static Block totem;
	
	//Runes
	public static Block runeAcid;
	public static Block runeChaos;
	public static Block runeDeath;
	public static Block runeDeseases;
	public static Block runeDivine;
	public static Block runeElectro;
	public static Block runeEvil;
	public static Block runeFear;
	public static Block runeFire;
	public static Block runeFrost;
	public static Block runeGood;
	public static Block runeLaw;
	public static Block runeMind;
	public static Block runeNegative;
	public static Block runePositive;
	public static Block runeSonic;

	// Items
	public static Item artronSword;
	public static Item brassIngot;
	public static Item brassNugget;
	public static Item catcher;
	public static Item crystal;
	public static Item darkDust;
	public static Item emeraldStaff;
	public static Item inactiveResonator;
	public static Item misteryCrystal;
	public static Item misteryCrystalCondenser;
	public static Item misteryCrystalCondenserActive;
	public static Item resonatingObscuraBucket;
	public static Item obscuriteChunk;
	public static Item resonator;
	public static Item sabre;
	public static Item stasis;
	public static Item strangeMatter;
	public static Item ultraCheese;
	public static Item xpStorage;

	// Spells
	public static Item spellAcidArrow;
	public static Item spellFireBall;
	public static Item spellFlameArrow;
	public static Item spellForceField;
	public static Item spellGasCloud;
	public static Item spellHealingAugmented;
	public static Item spellHealingCircle;
	public static Item spellHealingMundane;
	public static Item spellHealingNormal;
	public static Item spellIceStorm;
	public static Item spellLightningBolt;
	public static Item spellMeteoricStrike;
	public static Item spellWitheringTentacles;
	public static Item spellExplosion;

	// Fluids
	public static Fluid resonatingObscura;

	public static final Item.ToolMaterial obscuraToolMaterial = EnumHelper.addToolMaterial("OBSCURITE", 4, 2050, 5.1F,
			5, 20);

	public void RegisterRenderThings() {
	}

	private TickHandler serverTickHandler;
	
	public CommonProxy() {
	}

	public void preinit() {
		OSMMain.config.init();
		
		LogHelper.info("Pre-init state");

		// ___________________________________________BLOCKS

		LogHelper.info("Registering OSM blocks");
		
		abstractEffect = new AbstractEffect();
		GameRegistry.registerBlock(abstractEffect, abstractEffect.getUnlocalizedName().substring(5));
		
		beacore = new BeaCore();
		GameRegistry.registerBlock(beacore, beacore.getUnlocalizedName().substring(5));
		
//blackFlame = new BlackFlame();
//GameRegistry.registerBlock(blackFlame, blackFlame.getUnlocalizedName().substring(5));
		
		brassBlock = new BrassBlock();
		GameRegistry.registerBlock(brassBlock, brassBlock.getUnlocalizedName().substring(5));
		
		chronodialProcessor = new ChronodialProcessor();
		GameRegistry.registerBlock(chronodialProcessor, chronodialProcessor.getUnlocalizedName().substring(5));
		
		corupteCon = new CorupteCon();
		GameRegistry.registerBlock(corupteCon, corupteCon.getUnlocalizedName().substring(5));

		forceField = new ForceField();
		GameRegistry.registerBlock(forceField, forceField.getUnlocalizedName().substring(5));

		gasCloud = new GasCloud();
		GameRegistry.registerBlock(gasCloud, gasCloud.getUnlocalizedName().substring(5));

		magicalCasing = new MagicalCasing();
		GameRegistry.registerBlock(magicalCasing, magicalCasing.getUnlocalizedName().substring(5));
		
		obscuriteBlock = new ObscuriteBlock();
		GameRegistry.registerBlock(obscuriteBlock, obscuriteBlock.getUnlocalizedName().substring(5));

		obscuraCrystal = new ObscuraCrystal();
		GameRegistry.registerBlock(obscuraCrystal, obscuraCrystal.getUnlocalizedName().substring(5));

		obscuraGlass = new ObscuraGlass();
		GameRegistry.registerBlock(obscuraGlass, obscuraGlass.getUnlocalizedName().substring(5));

		obscuriteOre = new ObscuriteOre();
		GameRegistry.registerBlock(obscuriteOre, obscuriteOre.getUnlocalizedName().substring(5));

		obscuraSand = new MisteryDust();
		GameRegistry.registerBlock(obscuraSand, obscuraSand.getUnlocalizedName().substring(5));

		purpleCon = new PurpleCon();
		GameRegistry.registerBlock(purpleCon, purpleCon.getUnlocalizedName().substring(5));

		riteStone = new RitualStone();
		GameRegistry.registerBlock(riteStone, riteStone.getUnlocalizedName().substring(5));

		runicStone = new RunicStone();
		GameRegistry.registerBlock(runicStone, runicStone.getUnlocalizedName().substring(5));
		
		totem = new RitualTotem();
		GameRegistry.registerBlock(totem, totem.getUnlocalizedName().substring(5));
		
		// ___________________________________________RUNES
		
		runeAcid = new RuneAcid();
		GameRegistry.registerBlock(runeAcid, runeAcid.getUnlocalizedName().substring(5));
		
		runeChaos = new RuneChaos();
		GameRegistry.registerBlock(runeChaos, runeChaos.getUnlocalizedName().substring(5));
		
		runeDeath = new RuneDeath();
		GameRegistry.registerBlock(runeDeath, runeDeath.getUnlocalizedName().substring(5));
		
		runeDeseases = new RuneDeseases();
		GameRegistry.registerBlock(runeDeseases, runeDeseases.getUnlocalizedName().substring(5));
		
		runeDivine = new RuneDivine();
		GameRegistry.registerBlock(runeDivine, runeDivine.getUnlocalizedName().substring(5));
		
		runeElectro = new RuneElectro();
		GameRegistry.registerBlock(runeElectro, runeElectro.getUnlocalizedName().substring(5));
		
		runeEvil = new RuneEvil();
		GameRegistry.registerBlock(runeEvil, runeEvil.getUnlocalizedName().substring(5));
		
		runeFear = new RuneFear();
		GameRegistry.registerBlock(runeFear, runeFear.getUnlocalizedName().substring(5));
		
		runeFire = new RuneFire();
		GameRegistry.registerBlock(runeFire, runeFire.getUnlocalizedName().substring(5));
		
		runeFrost = new RuneFrost();
		GameRegistry.registerBlock(runeFrost, runeFrost.getUnlocalizedName().substring(5));
		
		runeGood = new RuneGood();
		GameRegistry.registerBlock(runeGood, runeGood.getUnlocalizedName().substring(5));
		
		runeLaw = new RuneLaw();
		GameRegistry.registerBlock(runeLaw, runeLaw.getUnlocalizedName().substring(5));
		
		runeMind = new RuneMind();
		GameRegistry.registerBlock(runeMind, runeMind.getUnlocalizedName().substring(5));
		
		runeNegative = new RuneNegative();
		GameRegistry.registerBlock(runeNegative, runeNegative.getUnlocalizedName().substring(5));
		
		runePositive = new RunePositive();
		GameRegistry.registerBlock(runePositive, runePositive.getUnlocalizedName().substring(5));
		
		runeSonic = new RuneSonic();
		GameRegistry.registerBlock(runeSonic, runeSonic.getUnlocalizedName().substring(5));
		

		// ___________________________________________ITEMS
		LogHelper.info("Registering OSM items");

		artronSword = new ArtronSword();
		GameRegistry.registerItem(artronSword, artronSword.getUnlocalizedName().substring(5));
		
		brassIngot = new BrassIngot();
		GameRegistry.registerItem(brassIngot, brassIngot.getUnlocalizedName().substring(5));
		
		brassNugget = new BrassNugget();
		GameRegistry.registerItem(brassNugget, brassNugget.getUnlocalizedName().substring(5));
		
		catcher = new Catcher();
		GameRegistry.registerItem(catcher, catcher.getUnlocalizedName().substring(5));
		
		crystal = new Crystal();
		GameRegistry.registerItem(crystal, crystal.getUnlocalizedName().substring(5));
		
		darkDust = new DarkDust();
		GameRegistry.registerItem(darkDust, darkDust.getUnlocalizedName().substring(5));

		emeraldStaff = new EmeraldStaff();
		GameRegistry.registerItem(emeraldStaff, emeraldStaff.getUnlocalizedName().substring(5));

		inactiveResonator = new InactiveResonator();
		GameRegistry.registerItem(inactiveResonator, inactiveResonator.getUnlocalizedName().substring(5));
		
		misteryCrystal = new MisteryCrystal();
		GameRegistry.registerItem(misteryCrystal, misteryCrystal.getUnlocalizedName().substring(5));
		
		misteryCrystalCondenser = new MisteryCrystalCondenser();
		GameRegistry.registerItem(misteryCrystalCondenser, misteryCrystalCondenser.getUnlocalizedName().substring(5));
		
		misteryCrystalCondenserActive = new MisteryCrystalCondenserActive();
		GameRegistry.registerItem(misteryCrystalCondenserActive, misteryCrystalCondenserActive.getUnlocalizedName().substring(5));
		
		obscuriteChunk = new ObscuriteChunk();
		GameRegistry.registerItem(obscuriteChunk, obscuriteChunk.getUnlocalizedName().substring(5));

		resonatingObscuraBucket = new ResonatingObscuraBucket();
		GameRegistry.registerItem(resonatingObscuraBucket, resonatingObscuraBucket.getUnlocalizedName().substring(5));

		resonator = new Resonator();
		GameRegistry.registerItem(resonator, resonator.getUnlocalizedName().substring(5));

		sabre = new Sabre();
		GameRegistry.registerItem(sabre, sabre.getUnlocalizedName().substring(5));
		
		stasis = new Stasis();
		GameRegistry.registerItem(stasis, stasis.getUnlocalizedName().substring(5));
		
		strangeMatter = new StrangeMatter();
		GameRegistry.registerItem(strangeMatter, strangeMatter.getUnlocalizedName().substring(5));

		ultraCheese = new UltraCheese();
		GameRegistry.registerItem(ultraCheese, ultraCheese.getUnlocalizedName().substring(5));

		xpStorage = new XPStorage();
		GameRegistry.registerItem(xpStorage, xpStorage.getUnlocalizedName().substring(5));

		
		// __________________________________________SPELLS
		spellAcidArrow = new AcidArrowSpell();
		GameRegistry.registerItem(spellAcidArrow, spellAcidArrow.getUnlocalizedName().substring(5));

		spellFireBall = new FireBallSpell();
		GameRegistry.registerItem(spellFireBall, spellFireBall.getUnlocalizedName().substring(5));

		spellFlameArrow = new FlameArrowSpell();
		GameRegistry.registerItem(spellFlameArrow, spellFlameArrow.getUnlocalizedName().substring(5));

		spellForceField = new ForceFieldSpell();
		GameRegistry.registerItem(spellForceField, spellForceField.getUnlocalizedName().substring(5));

		spellGasCloud = new GasCloudSpell();
		GameRegistry.registerItem(spellGasCloud, spellGasCloud.getUnlocalizedName().substring(5));

		spellLightningBolt = new LightningBoltSpell();
		GameRegistry.registerItem(spellLightningBolt, spellLightningBolt.getUnlocalizedName().substring(5));

		spellHealingAugmented = new HealingAugmentedSpell();
		GameRegistry.registerItem(spellHealingAugmented, spellHealingAugmented.getUnlocalizedName().substring(5));

		spellHealingCircle = new HealingCircleSpell();
		GameRegistry.registerItem(spellHealingCircle, spellHealingCircle.getUnlocalizedName().substring(5));

		spellHealingMundane = new HealingMundaneSpell();
		GameRegistry.registerItem(spellHealingMundane, spellHealingMundane.getUnlocalizedName().substring(5));

		spellHealingNormal = new HealingNormalSpell();
		GameRegistry.registerItem(spellHealingNormal, spellHealingNormal.getUnlocalizedName().substring(5));

		spellIceStorm = new IceStormSpell();
		GameRegistry.registerItem(spellIceStorm, spellIceStorm.getUnlocalizedName().substring(5));

		spellMeteoricStrike = new MeteoricStrikeSpell();
		GameRegistry.registerItem(spellMeteoricStrike, spellMeteoricStrike.getUnlocalizedName().substring(5));
		
		spellWitheringTentacles = new WitheringTentaclesSpell();
		GameRegistry.registerItem(spellWitheringTentacles, spellWitheringTentacles.getUnlocalizedName().substring(5));

		spellExplosion = new ExplosionSpell();
		GameRegistry.registerItem(spellExplosion, spellExplosion.getUnlocalizedName().substring(5));
		
		// __________________________________________FLUIDS

		LogHelper.info("Registering OSM fluids");

		resonatingObscura = new Fluid("ResonatingObscura");
        FluidRegistry.registerFluid(resonatingObscura);
        
        obscuraFluid = new ResonatingObscura(resonatingObscura, Material.water);
        GameRegistry.registerBlock(obscuraFluid, "HealingWater");
        
        resonatingObscura.setUnlocalizedName(obscuraFluid.getUnlocalizedName()).setDensity(500).setLuminosity(10).setTemperature(-50).setViscosity(500).setGaseous(true);

		LogHelper.info("Waiting for FMLInitialization event");
	}

	public void init() {
		LogHelper.info("Init state");

		// ______________________________________TILEENTITY
		LogHelper.info("Registering OSM TileEntities");
		
		GameRegistry.registerTileEntity(ObscuraCrystalTileEntity.class, "octe");
		GameRegistry.registerTileEntity(PurpleConTileEntity.class, "pcte");
		GameRegistry.registerTileEntity(RitualTotemTileEntity.class, "rtte");
		GameRegistry.registerTileEntity(ObscuraEngineTileEntity.class, "oete");
		GameRegistry.registerTileEntity(CorupteConTileEntity.class, "ccte");

		// _________________________________________RECIPES
		LogHelper.info("Registering OSM recipes");
		
		GameRegistry.addRecipe(new ItemStack(xpStorage, 1, 1024),
			new Object[] { " P ", "GNG", "GGG",
			'P',Blocks.planks, 'G', Blocks.glass, 'N', Items.nether_star });
		
		GameRegistry.addSmelting(obscuraSand, new ItemStack(obscuraGlass, 1), 0.5F);
		
		GameRegistry.addRecipe(new ItemStack(beacore),
			new Object[]{"OGO", "DND", "BEB",
			'B', Items.blaze_rod, 'D', Items.diamond, 'E', Items.ender_eye, 'G', obscuraGlass, 'N', Items.nether_star, 'O', obscuriteChunk});
		
		GameRegistry.addRecipe(new ItemStack(magicalCasing, 8), 
			new Object[] {"OGO", "GMG", "OGO",
			'G', obscuraGlass, 'M', strangeMatter, 'O', obscuriteChunk});
		
		GameRegistry.addShapelessRecipe(new ItemStack(obscuriteBlock), obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk, obscuriteChunk);
		
		GameRegistry.addShapelessRecipe(new ItemStack(obscuriteChunk, 9), obscuriteBlock);
		
		GameRegistry.addShapelessRecipe(new ItemStack(ultraCheese), Items.milk_bucket, obscuriteChunk);
		
		GameRegistry.addShapelessRecipe(new ItemStack(brassIngot), brassNugget, brassNugget, brassNugget, brassNugget, brassNugget, brassNugget, brassNugget, brassNugget, brassNugget);

		GameRegistry.addShapelessRecipe(new ItemStack(brassBlock), brassIngot, brassIngot, brassIngot, brassIngot, brassIngot, brassIngot, brassIngot, brassIngot, brassIngot);

		GameRegistry.addShapelessRecipe(new ItemStack(brassIngot, 9), brassBlock);
		
		GameRegistry.addShapelessRecipe(new ItemStack(brassNugget, 9), brassIngot);
		LogHelper.info("Waiting for FMLPostInitialization event");
	}

	public void postinit() {
		// __________________________________________EVENTS
		MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
		
		// __________________________________________OREGEN
		GameRegistry.registerWorldGenerator(new ObscuraGen(), 0);
	}

	public void InitializeAndRegisterHandlers() {
		serverTickHandler = new TickHandler();
		FMLCommonHandler.instance().bus().register(serverTickHandler);
		NetPacketHandler.INSTANCE.registerChannels(new PacketProcessor());
	}
}
