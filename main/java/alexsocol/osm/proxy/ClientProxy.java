package alexsocol.osm.proxy;

import alexsocol.osm.blocks.runes.tileentity.RuneAcidTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneChaosTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneDeathTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneDeseasesTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneDivineTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneElectroTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneEvilTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneFearTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneFireTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneFrostTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneGoodTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneLawTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneMindTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneNegativeTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RunePositiveTileEntity;
import alexsocol.osm.blocks.runes.tileentity.RuneSonicTileEntity;
import alexsocol.osm.blocks.tileentity.AbstractEffectTileEntity;
import alexsocol.osm.blocks.tileentity.CorupteConTileEntity;
import alexsocol.osm.blocks.tileentity.ObscuraCrystalTileEntity;
import alexsocol.osm.blocks.tileentity.PurpleConTileEntity;
import alexsocol.osm.blocks.tileentity.RitualStoneTileEntity;
import alexsocol.osm.blocks.tileentity.RitualTotemTileEntity;
import alexsocol.osm.entity.AcidArrowEntity;
import alexsocol.osm.entity.FlameArrowEntity;
import alexsocol.osm.renderer.AbstractEffectRender;
import alexsocol.osm.renderer.AcidArrowEntityRender;
import alexsocol.osm.renderer.CorupteConRender;
import alexsocol.osm.renderer.FlameArrowEntityRender;
import alexsocol.osm.renderer.ObscuraCrystalRender;
import alexsocol.osm.renderer.PurpleConRender;
import alexsocol.osm.renderer.RitualStoneRender;
import alexsocol.osm.renderer.RitualTotemRender;
import alexsocol.osm.renderer.runes.RuneAcidRender;
import alexsocol.osm.renderer.runes.RuneChaosRender;
import alexsocol.osm.renderer.runes.RuneDeathRender;
import alexsocol.osm.renderer.runes.RuneDeseasesRender;
import alexsocol.osm.renderer.runes.RuneDivineRender;
import alexsocol.osm.renderer.runes.RuneElectroRender;
import alexsocol.osm.renderer.runes.RuneEvilRender;
import alexsocol.osm.renderer.runes.RuneFearRender;
import alexsocol.osm.renderer.runes.RuneFireRender;
import alexsocol.osm.renderer.runes.RuneFrostRender;
import alexsocol.osm.renderer.runes.RuneGoodRender;
import alexsocol.osm.renderer.runes.RuneLawRender;
import alexsocol.osm.renderer.runes.RuneMindRender;
import alexsocol.osm.renderer.runes.RuneNegativeRender;
import alexsocol.osm.renderer.runes.RunePositiveRender;
import alexsocol.osm.renderer.runes.RuneSonicRender;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {

	@Override
	public void RegisterRenderThings() {
		//Blocks
		ClientRegistry.bindTileEntitySpecialRenderer(AbstractEffectTileEntity.class, new AbstractEffectRender());
		ClientRegistry.bindTileEntitySpecialRenderer(CorupteConTileEntity.class, new CorupteConRender());
		ClientRegistry.bindTileEntitySpecialRenderer(ObscuraCrystalTileEntity.class, new ObscuraCrystalRender());
		ClientRegistry.bindTileEntitySpecialRenderer(PurpleConTileEntity.class, new PurpleConRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RitualStoneTileEntity.class, new RitualStoneRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RitualTotemTileEntity.class, new RitualTotemRender());

		//Runes
		ClientRegistry.bindTileEntitySpecialRenderer(RuneAcidTileEntity.class, new RuneAcidRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneChaosTileEntity.class, new RuneChaosRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneDeathTileEntity.class, new RuneDeathRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneDeseasesTileEntity.class, new RuneDeseasesRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneDivineTileEntity.class, new RuneDivineRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneElectroTileEntity.class, new RuneElectroRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneEvilTileEntity.class, new RuneEvilRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneFearTileEntity.class, new RuneFearRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneFireTileEntity.class, new RuneFireRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneFrostTileEntity.class, new RuneFrostRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneGoodTileEntity.class, new RuneGoodRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneLawTileEntity.class, new RuneLawRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneMindTileEntity.class, new RuneMindRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneNegativeTileEntity.class, new RuneNegativeRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RunePositiveTileEntity.class, new RunePositiveRender());
		ClientRegistry.bindTileEntitySpecialRenderer(RuneSonicTileEntity.class, new RuneSonicRender());
		
		//Entities
		RenderingRegistry.registerEntityRenderingHandler(AcidArrowEntity.class, new AcidArrowEntityRender());
		RenderingRegistry.registerEntityRenderingHandler(FlameArrowEntity.class, new FlameArrowEntityRender());
	}
}